-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tlcmap
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB-1:10.4.11+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `tlcmap`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tlcmap` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `tlcmap`;

--
-- Table structure for table `collablink`
--

DROP TABLE IF EXISTS `collablink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collablink` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dataset_id` bigint(20) NOT NULL,
  `link` varchar(255) NOT NULL,
  `dsrole` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collablink`
--

LOCK TABLES `collablink` WRITE;
/*!40000 ALTER TABLE `collablink` DISABLE KEYS */;
INSERT INTO `collablink` VALUES (66,33,'iUhJ0J078dUOzw6L6j7639bZ9','VIEWER','2020-02-23 05:33:00','2020-02-23 05:33:00');
/*!40000 ALTER TABLE `collablink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dataitem`
--

DROP TABLE IF EXISTS `dataitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataitem` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dataset_id` bigint(20) unsigned DEFAULT NULL,
  `placename` text NOT NULL,
  `description` text DEFAULT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `datestart` text DEFAULT NULL,
  `dateend` text DEFAULT NULL,
  `state` text DEFAULT NULL,
  `feature_term` text DEFAULT NULL,
  `lga` text DEFAULT NULL,
  `parish` text DEFAULT NULL,
  `source` text DEFAULT NULL,
  `external_url` text DEFAULT NULL,
  `extended_data` text DEFAULT NULL,
  `kml_style_url` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dataset_id` (`dataset_id`),
  CONSTRAINT `fk_dataset_id` FOREIGN KEY (`dataset_id`) REFERENCES `dataset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=829 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dataitem`
--

LOCK TABLES `dataitem` WRITE;
/*!40000 ALTER TABLE `dataitem` DISABLE KEYS */;
INSERT INTO `dataitem` VALUES (513,33,'z',NULL,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-01-21 01:43:19','2020-01-21 01:43:19'),(543,33,'testtestsetse',NULL,123,321,'2020-02-01','2020-02-29',NULL,NULL,NULL,NULL,NULL,'12312312',NULL,NULL,'2020-02-12 12:48:46','2020-02-12 14:09:07'),(566,33,'u',NULL,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:00:44','2020-02-12 15:00:44'),(567,33,'v',NULL,3,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:00:44','2020-02-12 15:00:44'),(568,33,'w',NULL,5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:00:44','2020-02-12 15:00:44'),(569,33,'x',NULL,7,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:00:44','2020-02-12 15:00:44'),(570,33,'y',NULL,9,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:00:44','2020-02-12 15:00:44'),(573,33,'c',NULL,5,6,'-16112-11-31','2020-07-29','NFK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-09-23 16:54:06'),(574,33,'d',NULL,7,8,'-16999-12-13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-08-27 12:37:00'),(575,33,'e',NULL,9,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(576,33,'f',NULL,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(577,33,'g',NULL,3,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(578,33,'h',NULL,5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(579,33,'i',NULL,7,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(580,33,'j',NULL,9,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(581,33,'k',NULL,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(582,33,'l',NULL,3,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(583,33,'m',NULL,5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(584,33,'n',NULL,7,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(585,33,'o',NULL,9,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(586,33,'p',NULL,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(587,33,'q',NULL,3,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(588,33,'r',NULL,5,6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(589,33,'s',NULL,7,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(590,33,'t',NULL,9,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-12 15:03:02','2020-02-12 15:03:02'),(701,33,'newplace1',NULL,876,876,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-26 23:48:43','2020-02-26 23:48:43'),(702,33,'newcastle','From Public Dataset!',1,3,NULL,NULL,'NSW','town','NEWCASTLE','NEWCASTLE','test','test',NULL,NULL,'2020-02-26 23:52:24','2020-02-27 03:56:29'),(703,67,'private',NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-27 04:02:30','2020-02-27 04:02:30'),(704,66,'nwecastle',NULL,123,123,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-03 21:43:50','2020-03-03 21:43:50'),(707,33,'NFK',NULL,123,123,NULL,NULL,'NFK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-05 07:19:30','2020-03-05 07:19:30'),(708,33,'DFGHJFGHJFGHJFGHJFGHJ',NULL,646546000,456456000,NULL,NULL,'VIC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-05 07:24:17','2020-03-05 07:24:17'),(775,74,'Coal River',NULL,55,-66,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-11 17:35:11','2020-06-11 17:35:11'),(776,75,'Kurri',NULL,122,122,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 14:00:57','2020-06-24 14:00:57'),(777,73,'','',-32.7384,151.549,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:30:44','2020-06-24 15:30:44'),(778,73,'','',-32.994,151.598,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:30:44','2020-06-24 15:30:44'),(779,73,'','',-32.9283,151.782,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:30:44','2020-06-24 15:30:44'),(780,73,'','',-32.8142,151.489,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:30:44','2020-06-24 15:30:44'),(781,73,'','',-19.259,146.817,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:30:44','2020-06-24 15:30:44'),(782,73,'','',-28.2646,153.578,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:30:44','2020-06-24 15:30:44'),(783,73,'','',-32.7384,151.549,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:31:05','2020-06-24 15:31:05'),(784,73,'','',-32.994,151.598,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:31:05','2020-06-24 15:31:05'),(785,73,'','',-32.9283,151.782,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:31:05','2020-06-24 15:31:05'),(786,73,'','',-32.8142,151.489,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:31:05','2020-06-24 15:31:05'),(787,73,'','',-19.259,146.817,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:31:05','2020-06-24 15:31:05'),(788,73,'','',-28.2646,153.578,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-24 15:31:05','2020-06-24 15:31:05'),(789,73,'Nowra','stuff happened here',-34.8878,150.616,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-16 11:55:02','2020-07-16 11:55:02'),(790,73,'Kangaroo Valley','here',-34.7322,150.524,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-16 11:55:02','2020-07-16 11:55:02'),(791,73,'Port Kembla','dfds',-34.4926,150.894,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-07-16 11:55:02','2020-07-16 11:55:02'),(792,66,'test',NULL,123,1234,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-28 21:31:12','2020-09-28 21:31:17'),(793,76,'',NULL,123.45,45.66,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-21 10:47:08','2020-10-21 10:47:08'),(794,76,'',NULL,-29.2481,153.995,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-21 10:47:08','2020-10-21 10:47:08'),(795,76,'',NULL,-29.9549,150.238,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-21 10:47:08','2020-10-21 10:47:08'),(796,76,'',NULL,123.45,45.66,'2020-09-28','2020-10-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-21 10:48:33','2020-10-21 10:48:33'),(797,76,'',NULL,-29.2481,153.995,'2020-10-01','2020-10-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-21 10:48:33','2020-10-21 10:48:33'),(798,76,'',NULL,-29.9549,150.238,'2020-09-28','2020-09-29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-21 10:48:33','2020-10-21 10:48:33'),(803,74,'gfhfgh',NULL,44,11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-28 22:22:18','2020-10-28 22:22:18'),(804,77,'London','',51.5074,-0.127758,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(805,77,'Isfahan Province','',33.2771,52.3613,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(806,77,'Istanbul','',41.0082,28.9784,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(807,77,'Athens','',37.9838,23.7275,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(808,77,'Kathmandu','',27.7172,85.324,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(809,77,'Jammu and Kashmir','',33.7782,76.5762,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(810,77,'Goa','',15.2993,74.124,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(811,77,'Yangon','',16.8661,96.1951,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(812,77,'Singapore','',1.35208,103.82,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(813,77,'Darwin','',-12.4634,130.846,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(814,77,'Kolkata','',22.5726,88.3639,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(815,77,'Lebanon','',33.8547,35.8623,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(816,77,'Israel','',31.0461,34.8516,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(817,77,'Marrakesh','',31.6295,-7.98108,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(818,77,'Sri Lanka','',7.87305,80.7718,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(819,77,'Dhaka','',23.8103,90.4125,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(820,77,'Bangkok','',13.7563,100.502,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(821,77,'Delhi','',28.6619,77.2274,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(822,77,'Peshawar','',34.0151,71.5249,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(823,77,'Varanasi','',25.3176,82.9739,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(824,77,'Kabul','',34.5554,69.2075,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(825,77,'Herat','',34.3529,62.204,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(826,77,'Tehran','',35.6892,51.389,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<styleUrl>#icon-1899-0288D1-nodesc</styleUrl>','2020-12-10 18:17:31','2020-12-10 18:17:31'),(827,78,'Dabee','Village of Dabee, present in a series of maps in the NSW Historical Land Records Viewer',-32.7777,150.022,'1884-01-01','1936-01-01','NSW','village','RYLSTONE','Dabee',NULL,NULL,NULL,NULL,'2020-12-10 18:45:33','2020-12-10 18:45:33'),(828,79,'Bob',NULL,3,44,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-10 19:12:51','2020-12-10 19:12:51');
/*!40000 ALTER TABLE `dataitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dataset`
--

DROP TABLE IF EXISTS `dataset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataset` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `allowANPS` tinyint(1) NOT NULL DEFAULT 0,
  `creator` text DEFAULT NULL,
  `publisher` text DEFAULT NULL,
  `contact` text DEFAULT NULL,
  `source` text DEFAULT NULL,
  `latitude_from` float DEFAULT NULL,
  `longitude_from` float DEFAULT NULL,
  `latitude_to` float DEFAULT NULL,
  `longitude_to` float DEFAULT NULL,
  `temporal_from` date DEFAULT NULL,
  `temporal_to` date DEFAULT NULL,
  `created` date DEFAULT NULL,
  `language` text DEFAULT NULL,
  `license` text DEFAULT NULL,
  `rights` text DEFAULT NULL,
  `kml_style` text DEFAULT NULL,
  `kml_journey` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dataset`
--

LOCK TABLES `dataset` WRITE;
/*!40000 ALTER TABLE `dataset` DISABLE KEYS */;
INSERT INTO `dataset` VALUES (33,'asd','example description',0,1,'adfs','asdf','asd',NULL,1,2,3,4,'2020-02-01','2020-02-02','2019-10-04','asd','asd','asdf',NULL,NULL,'2020-01-16 05:55:21','2020-12-06 19:17:13'),(62,'Successfully Edited','asdfads',0,0,'Ben','TLCMap','ben.mcdonnell@newcastle.edu.au',NULL,0,0,100,100,NULL,NULL,NULL,'English','none','none',NULL,NULL,'2020-01-20 02:28:10','2020-02-20 04:47:20'),(66,'Public','Testing public on create',0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-23 02:55:35','2020-12-06 19:17:27'),(67,'Private','Testing private on create',0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-23 02:55:51','2020-02-27 04:02:30'),(73,'Alice test','Hunter valley places',0,0,'Alice','?','Alice',NULL,NULL,NULL,NULL,NULL,'2020-06-20','2020-06-18','2020-06-03',NULL,NULL,NULL,NULL,NULL,'2020-06-03 17:03:32','2020-07-16 11:55:02'),(74,'Test','test',0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-11 17:34:27','2020-10-28 22:22:18'),(75,'Kurri Places','places mentioning Kurri Kurri',0,0,'Alice Jackson',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'English','Open','Open',NULL,NULL,'2020-06-24 13:56:52','2020-06-24 14:00:57'),(76,'Invalid Date test','test',0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-21 10:47:01','2020-10-21 10:48:33'),(77,'Test Dataset','For testing',1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<Style id=\"icon-1899-0288D1-nodesc-normal\">\n      <IconStyle>\n        <color>ff0000ff</color>\n        <scale>1</scale>\n        <Icon>\n          <href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href>\n        </Icon>\n        <hotSpot x=\"32\" xunits=\"pixels\" y=\"64\" yunits=\"insetPixels\"/>\n      </IconStyle>\n      <LabelStyle>\n        <scale>0</scale>\n      </LabelStyle>\n      <BalloonStyle>\n        <text><![CDATA[<h3>$[name]</h3>]]></text>\n      </BalloonStyle>\n    </Style><Style id=\"icon-1899-0288D1-nodesc-highlight\">\n      <IconStyle>\n        <color>ffd18802</color>\n        <scale>1</scale>\n        <Icon>\n          <href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href>\n        </Icon>\n        <hotSpot x=\"32\" xunits=\"pixels\" y=\"64\" yunits=\"insetPixels\"/>\n      </IconStyle>\n      <LabelStyle>\n        <scale>1</scale>\n      </LabelStyle>\n      <BalloonStyle>\n        <text><![CDATA[<h3>$[name]</h3>]]></text>\n      </BalloonStyle>\n    </Style><StyleMap id=\"icon-1899-0288D1-nodesc\">\n      <Pair>\n        <key>normal</key>\n        <styleUrl>#icon-1899-0288D1-nodesc-normal</styleUrl>\n      </Pair>\n      <Pair>\n        <key>highlight</key>\n        <styleUrl>#icon-1899-0288D1-nodesc-highlight</styleUrl>\n      </Pair>\n    </StyleMap>',NULL,'2020-12-10 18:16:05','2020-12-10 18:24:21'),(78,'Village of Dabee','Location of the village of Dabee',1,1,'Kimberley Connor',NULL,'kconnor@stanford.edu','LTO Charting Map, County of Phillip, Parish of Dabee',-32.7868,150.017,-32.7821,150.018,'1884-01-01','1936-01-01','2020-12-10',NULL,NULL,NULL,NULL,NULL,'2020-12-10 18:41:22','2020-12-10 18:45:33'),(79,'asd','sadfsw',0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-10 19:12:23','2020-12-10 19:12:51');
/*!40000 ALTER TABLE `dataset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dataset_subject_keyword`
--

DROP TABLE IF EXISTS `dataset_subject_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataset_subject_keyword` (
  `dataset_id` bigint(20) unsigned NOT NULL,
  `subject_keyword_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`dataset_id`,`subject_keyword_id`),
  KEY `dataset_subject_keyword_subject_keyword_id_foreign` (`subject_keyword_id`),
  CONSTRAINT `dataset_subject_keyword_dataset_id_foreign` FOREIGN KEY (`dataset_id`) REFERENCES `dataset` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dataset_subject_keyword_subject_keyword_id_foreign` FOREIGN KEY (`subject_keyword_id`) REFERENCES `subject_keyword` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dataset_subject_keyword`
--

LOCK TABLES `dataset_subject_keyword` WRITE;
/*!40000 ALTER TABLE `dataset_subject_keyword` DISABLE KEYS */;
INSERT INTO `dataset_subject_keyword` VALUES (33,9),(33,19),(62,9),(62,16),(62,17),(62,20),(66,21),(67,22),(73,31),(74,9),(74,32),(75,10),(75,31),(75,33),(75,34),(76,9),(77,35),(78,36),(78,37),(78,38),(79,23);
/*!40000 ALTER TABLE `dataset_subject_keyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('ian.johnson@sydney.edu.au','$2y$10$G1NHptp3Cb0ERKLzE7.KXuUZfHgE/wZiNZUzhkjzuuZbI7nJTNBrS','2020-11-03 15:21:45');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'REGULAR','A regular user, can only browse',NULL,NULL),(2,'ADMIN','An admin, can browse, add content, edit content, manage users below them',NULL,NULL),(3,'SUPER_ADMIN','Complete control over entire system and users',NULL,NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,3,5),(2,2,23),(3,1,8),(4,1,22),(6,2,12),(7,1,14),(8,1,13),(9,2,6),(10,1,9),(11,1,20),(12,1,15),(13,1,16),(14,1,17),(15,1,21),(16,1,18),(17,1,19),(18,1,3),(19,1,4),(20,1,2),(21,1,11),(22,1,7),(23,1,1),(24,1,3),(25,1,10),(26,1,24),(27,1,25),(28,1,26),(29,1,27),(30,1,28);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_search`
--

DROP TABLE IF EXISTS `saved_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_search` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `query` text NOT NULL,
  `count` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_search`
--

LOCK TABLES `saved_search` WRITE;
/*!40000 ALTER TABLE `saved_search` DISABLE KEYS */;
INSERT INTO `saved_search` VALUES (53,23,'?anps_id=1&leaflet-base-layers_45=on',1,'1','2020-02-11 21:19:57','2020-02-11 21:19:57'),(64,5,'?direction=asc&from=1&sort=anps_id&to=1000',809,'test','2020-03-11 23:30:31','2020-03-11 23:30:31'),(65,5,'?direction=asc&from=1&sort=anps_id&to=1000',809,'tset','2020-03-11 23:32:05','2020-03-11 23:32:05'),(66,5,'?direction=asc&from=1&sort=anps_id&to=1000',809,'tsetse','2020-03-11 23:33:29','2020-03-11 23:33:29'),(68,3,'?circle=151.682224%2C-32.891984%2C18716.03060370035&fuzzyname=Mayfield&leaflet-base-layers_57=on&searchausgaz=on&searchpublicdatasets=on',6,NULL,'2020-03-25 06:22:09','2020-03-25 06:22:09'),(69,3,'?circle=151.781101%2C-32.928877%2C8267.188862354227&fuzzyname=cook&leaflet-base-layers_57=on&searchausgaz=on&searchpublicdatasets=on',2,'coook','2020-03-25 06:24:58','2020-03-25 06:24:58'),(70,3,'?leaflet-base-layers_57=on&name=gadara&searchausgaz=on&searchpublicdatasets=on',8,'gas','2020-03-25 06:27:09','2020-03-25 06:27:09'),(71,4,'?fuzzyname=coquun&leaflet-base-layers_57=on&searchausgaz=on&searchpublicdatasets=on',176,'coquun live test','2020-03-29 19:58:01','2020-03-29 19:58:01'),(72,7,'?fuzzyname=exeter&leaflet-base-layers_57=on&searchausgaz=on&searchpublicdatasets=on',22,'exeter','2020-05-02 04:50:03','2020-05-02 04:50:03'),(73,1,'?fuzzyname=hunter%20valley&leaflet-base-layers_56=on&searchausgaz=on&searchpublicdatasets=on',2,'Hunter valley','2020-05-13 16:55:18','2020-05-13 16:55:18'),(74,1,'?from=3825&leaflet-base-layers_57=on&searchausgaz=on&searchpublicdatasets=on&to=3900',70,'alice test','2020-06-03 16:09:06','2020-06-03 16:09:06'),(75,1,'?leaflet-base-layers_57=on&lga=AUBURN&searchausgaz=on&searchpublicdatasets=on',90,'Auburn','2020-06-03 17:10:24','2020-06-03 17:10:24'),(76,1,'?circle=149.913425%2C-32.531473%2C88586.49866850369&leaflet-base-layers_57=on&searchausgaz=on&searchpublicdatasets=on',4757,'alice test','2020-07-14 15:53:45','2020-07-14 15:53:45'),(77,2,'?fuzzyname=Newcastle&leaflet-base-layers_57=on&searchausgaz=on&searchpublicdatasets=on',70,'newc test','2020-09-23 15:37:19','2020-09-23 15:37:19');
/*!40000 ALTER TABLE `saved_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_keyword`
--

DROP TABLE IF EXISTS `subject_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject_keyword` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_keyword`
--

LOCK TABLES `subject_keyword` WRITE;
/*!40000 ALTER TABLE `subject_keyword` DISABLE KEYS */;
INSERT INTO `subject_keyword` VALUES (9,'test'),(10,'australia'),(11,'aaaa'),(12,'nsw'),(13,'another tag'),(14,'live'),(15,'edit'),(16,'new'),(17,'tag'),(18,'redirect'),(19,'test2'),(20,'newtag'),(21,'public'),(22,'private'),(23,'asdf'),(24,'omitted'),(25,'unmapped'),(26,'indigenous'),(27,'aboriginal'),(28,'kabi kabi'),(29,'dreaming'),(30,'se qld'),(31,'hunter valley'),(32,'te'),(33,'kurri kurri'),(34,'new south wales'),(35,'humanities'),(36,'rural nsw'),(37,'settlers'),(38,'pastoral');
/*!40000 ALTER TABLE `subject_keyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testplaces`
--

DROP TABLE IF EXISTS `testplaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testplaces` (
  `name` text NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testplaces`
--

LOCK TABLES `testplaces` WRITE;
/*!40000 ALTER TABLE `testplaces` DISABLE KEYS */;
INSERT INTO `testplaces` VALUES ('test1',100,55),('test2',100.05,55.24),('test3',99,50);
/*!40000 ALTER TABLE `testplaces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` date DEFAULT NULL,
  `password` text NOT NULL,
  `remember_token` text DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp(),
  `created_at` datetime DEFAULT current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Alice Jackson','alice.jackson@newcastle.edu.au','2020-06-24','$2y$10$DEOjBnktwaBYcESnP8j8w.1RQE/yDQkEehPo6Q0y3bL/GetECCvb6','HU7ejqApyWYGR9tmJfXpUXf3iRIOL34KbB2VDzOsLr6vM0SgESnBwss4PuVu','2020-06-24 13:46:57','2020-05-13 15:43:01',1),(2,'bill.pascoe@newcastle.edu.au','bill.pascoe@newcastle.edu.au','2020-06-11','$2y$10$/jLQT.zNYHB2G/alkwtY3ebpIO.iaeIOeN/FIsLVyNgAAact7diRK','yhto7FhWaHVnf0OPGnTCgYo3OJOKlMLaAFowksjSC6QvPn228T3jTpwTZk1k','2020-06-11 18:04:53','2020-03-30 00:55:49',1),(3,'Will','bill_pascoe@yahoo.com','2020-06-11','$2y$10$2487QbyDyvqunGzMeZKZ3uWyOBnRP3Is1xxLwDidDkg8j19/gE4TO','h2S7FUUIivsRBaPEUd2QrEvLIAHXzVcj6ceuVPhunYFRNN78KcijcKTt2Zqc','2020-11-04 10:51:35','2020-06-11 18:29:53',1),(4,'Bill','bpasco2@gmail.com',NULL,'$2y$10$0dHdJaTUodlnEAg6yhn0WOVY1fUZwPHJWMmEJM/q0lcuEsY8YQpMW',NULL,'2020-03-29 19:57:25','2020-03-29 19:57:25',1),(5,'ben','c3166457@uon.edu.au','2020-08-20','$2y$10$1m66W8VzOpog7gWLDZkpnO8GPT4HoorKMGWXWd4RUXg67TJrngIZS','hiTBz5vMPVmaCmYbpXXySVxmYDWhLGEBxhfBiKqYCH7jT4MmzuNiB0yEBCAo','2020-09-23 16:53:24','2019-11-14 00:00:00',1),(6,'Testing Dates','date@test.com',NULL,'$2y$10$AJWhqUy.oOa4yS5DIUZGvu9ykTTYi4/7RIXkIoYRH9a2x352Mx.kW',NULL,'2020-02-13 03:17:24','2020-02-13 01:36:26',1),(7,'Ian Johnson','ian.johnson@sydney.edu.au',NULL,'$2y$10$ZDnDPfyFpG.SQJq777C1fODYFmCjuNOCOm7B0yJvJUmk3l65BDOOW','4QaoOkPNbX6uZevwXVYeOBVfa9Upfw8BBQiozpdySV0GaS8knSAQszNj9pfV','2020-05-02 04:49:49','2020-05-02 04:49:49',1),(8,'inactive','inactive@abc.com',NULL,'$2y$10$TZKQ3Y9H535prszs7QIsN..TV3MgkM.iOcr./Ahx7DX3AoWlXP9aS',NULL,'2019-11-22 00:00:00','2019-11-21 00:00:00',0),(9,'jointest','join@test.com',NULL,'$2y$10$aLGjWSUKuvCzUfWZXCO6M.A5Lsc.jDp1BzMy6jXKQcRG.ZpTF7tAO',NULL,'2020-02-23 14:49:25','2020-02-23 14:49:25',1),(10,'bernaldinobest https://wikipedia.org 7825169','juliagaskoin95@yandex.ru',NULL,'$2y$10$Fw182X3krHfDkz5Q6.k0yewEniMktDIfbT/v9Lv1mpqWwP8RqE1CS',NULL,'2020-08-19 19:41:54','2020-08-19 19:41:54',1),(11,'Monika Schwarz','monika.schwarz@monash.edu',NULL,'$2y$10$RqJjWqAjkrRX5ydiKjqchuHWTAFxJiAsFG7FcHpnKrZhNxx1.uUH6',NULL,'2020-04-08 13:33:02','2020-04-08 13:33:02',1),(12,'new','new@new.new',NULL,'$2y$10$Ot/GTqmqAxel1gMyHWZKJeFs8OOA52weNlgxj/syxRjB/ZYeueIfi',NULL,'2020-01-20 00:00:00','2019-12-05 00:00:00',1),(13,'newnewname','newnew@name.com',NULL,'$2y$10$jofZxcNxxZrJ6msNIvLO5OaYlD9ujT/Jghhor7xXAhQX/2EpGvY3u',NULL,'2020-01-29 00:00:00','2020-01-29 00:00:00',1),(14,'password','password@password.com',NULL,'$2y$10$/lvghK/gOaNUKxvHsKR1mesdsfrNtqLx46sf5VLThVlHjVwjoyOTK',NULL,'2020-01-13 00:00:00','2020-01-13 00:00:00',1),(15,'redirecttest2','redirect2@test.com',NULL,'$2y$10$LXDq8wN7srZIFfNyNBDcL.uBZpnzlbpCbI9Xlmvl/PAlvSpRv9FYq',NULL,'2020-02-23 14:55:19','2020-02-23 14:55:19',1),(16,'redirecttest3','redirect3@test.com',NULL,'$2y$10$xn5xpU6SEvLTXm6HiwEu.uWd6fzFCm2/xBR/.0bR.4IZr8cQgUXDu',NULL,'2020-02-23 14:56:15','2020-02-23 14:56:15',1),(17,'redirecttest4','redirect4@test.com',NULL,'$2y$10$wYVPvqhJ0eA8jlFHlf6J2OBnIXtn7ePCFZQa/0ykfHo/8LcHMDGCq',NULL,'2020-02-23 14:59:25','2020-02-23 14:59:25',1),(18,'redirecttest6','redirect6@test.com',NULL,'$2y$10$AF0FEYaFxnjIVPsqaFI8WOJWjFjV0/e4YZICpzfsHZiydxcAhcIuu',NULL,'2020-02-23 15:01:15','2020-02-23 15:01:15',1),(19,'redirecttest7','redirect7@test.com',NULL,'$2y$10$bDB5cBgigwDYNJnafIbBYuR0RuHzYFJpVIs2BBUbS96j6ob/qTlSe',NULL,'2020-02-23 15:02:10','2020-02-23 15:02:10',1),(20,'redirecttest','redirect@test.com',NULL,'$2y$10$LFGfzN.n0JYm9a5td.9JWOvMWlZeVhPk/2pwOdVQGkL4i9urmRaS2',NULL,'2020-02-23 14:54:39','2020-02-23 14:54:39',1),(21,'registertest5','register5@test.com',NULL,'$2y$10$8RM16UpxH40lwkOGo3qk6.Lm2RhGFRu.GrYapwGYkDDMCCNIoJPWu',NULL,'2020-02-23 14:59:57','2020-02-23 14:59:57',1),(22,'regularuser','regularuser@abc.com',NULL,'$2y$10$6um3EvVKn1UeR4/XpF8yZOADOZcy0QZpcXVGUkX7g4mrSIXOrjIPe','vYaqhnhR8J4u13POVtTc6qfET6GY1nxjUN1eCwl5Bgi40YX5hjCTEo3rb0eg','2019-11-27 00:00:00','2019-11-27 00:00:00',1),(23,'test','test@test.test',NULL,'$2y$10$PjmHbM.2Hq6Fgi7qr/huUuXmSjMYqeRya1xX5bEQ79P2qaz6mJqMO','e17TDhP24BHJSXKH5ZP5HBjnyypMztartVbPpGBHaNitJsugazj8aheTj3Bq','2020-02-13 00:00:00','2019-11-21 00:00:00',1),(24,'bpascoe','bpasco3@gmail.com','2020-12-06','$2y$10$mG94Tf4w01kDJFKbGcJrNePmTDPqpHS67Q1lJ1uzYd6fQ0OPgwtTW',NULL,'2020-12-06 21:09:25','2020-12-06 20:41:20',1),(25,'Hugh Craig','hugh.craig@newcastle.edu.au','2020-12-10','$2y$10$1bQacrmSCbT.AiW46dykE.L2tTg1pva8jWQob4UbR5OXc6.nCSxvy',NULL,'2020-12-10 12:02:22','2020-12-10 12:00:07',1),(26,'Kimberley Connor','kconnor@stanford.edu','2020-12-10','$2y$10$Tf6GFwxcWojy5WnOY6LYJ.WJiOVlhSNuz.gyHY6C6McbpjWsjdelW',NULL,'2020-12-10 18:34:22','2020-12-10 18:34:06',1),(27,'Kerri Hillsdon','20042484@student.westernsydney.edu.au','2020-12-19','$2y$10$Hw9VnHUcsa8eHkEpse9eJefAdoB49scY0JLTk8Fv9hE.HPyYMCx3S',NULL,'2020-12-19 16:39:17','2020-12-19 16:38:14',1),(28,'Sadaf','sadafakhalil@gmail.com','2020-12-20','$2y$10$F9P.kqdNV3pR.OvQmeVUI.tOVdpl2xV/f3Gb2Uyu5jXt/4xtSYMuS',NULL,'2020-12-20 15:40:19','2020-12-20 15:39:53',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_dataset`
--

DROP TABLE IF EXISTS `user_dataset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_dataset` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `dataset_id` bigint(20) unsigned NOT NULL,
  `dsrole` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_dataset`
--

LOCK TABLES `user_dataset` WRITE;
/*!40000 ALTER TABLE `user_dataset` DISABLE KEYS */;
INSERT INTO `user_dataset` VALUES (124,5,33,'OWNER','2020-01-16 05:55:21','2020-01-16 05:55:21'),(125,5,34,'OWNER','2020-01-20 01:29:48','2020-01-20 01:29:48'),(126,5,35,'OWNER','2020-01-20 01:30:26','2020-01-20 01:30:26'),(127,5,36,'OWNER','2020-01-20 01:31:32','2020-01-20 01:31:32'),(128,5,37,'OWNER','2020-01-20 01:34:30','2020-01-20 01:34:30'),(129,5,38,'OWNER','2020-01-20 01:35:07','2020-01-20 01:35:07'),(130,5,39,'OWNER','2020-01-20 01:37:42','2020-01-20 01:37:42'),(131,5,40,'OWNER','2020-01-20 01:37:58','2020-01-20 01:37:58'),(132,5,41,'OWNER','2020-01-20 01:43:01','2020-01-20 01:43:01'),(133,5,42,'OWNER','2020-01-20 01:47:03','2020-01-20 01:47:03'),(134,5,43,'OWNER','2020-01-20 01:48:29','2020-01-20 01:48:29'),(135,5,44,'OWNER','2020-01-20 01:54:12','2020-01-20 01:54:12'),(136,5,45,'OWNER','2020-01-20 01:55:05','2020-01-20 01:55:05'),(137,5,46,'OWNER','2020-01-20 01:57:05','2020-01-20 01:57:05'),(138,5,47,'OWNER','2020-01-20 01:58:41','2020-01-20 01:58:41'),(139,5,48,'OWNER','2020-01-20 02:01:03','2020-01-20 02:01:03'),(140,5,49,'OWNER','2020-01-20 02:02:11','2020-01-20 02:02:11'),(141,5,50,'OWNER','2020-01-20 02:05:18','2020-01-20 02:05:18'),(142,5,51,'OWNER','2020-01-20 02:06:33','2020-01-20 02:06:33'),(143,5,52,'OWNER','2020-01-20 02:09:09','2020-01-20 02:09:09'),(144,5,53,'OWNER','2020-01-20 02:09:53','2020-01-20 02:09:53'),(145,5,54,'OWNER','2020-01-20 02:10:41','2020-01-20 02:10:41'),(146,5,55,'OWNER','2020-01-20 02:11:32','2020-01-20 02:11:32'),(147,5,56,'OWNER','2020-01-20 02:12:41','2020-01-20 02:12:41'),(148,5,57,'OWNER','2020-01-20 02:13:13','2020-01-20 02:13:13'),(149,5,58,'OWNER','2020-01-20 02:21:19','2020-01-20 02:21:19'),(150,5,59,'OWNER','2020-01-20 02:23:21','2020-01-20 02:23:21'),(151,5,60,'OWNER','2020-01-20 02:25:51','2020-01-20 02:25:51'),(152,5,61,'OWNER','2020-01-20 02:26:58','2020-01-20 02:26:58'),(153,5,62,'OWNER','2020-01-20 02:28:10','2020-01-20 02:28:10'),(161,5,66,'OWNER','2020-02-23 02:55:35','2020-02-23 02:55:35'),(162,5,67,'OWNER','2020-02-23 02:55:51','2020-02-23 02:55:51'),(173,23,33,'VIEWER','2020-02-23 05:32:39','2020-02-23 05:32:39'),(179,1,73,'OWNER','2020-06-03 17:03:33','2020-06-03 17:03:33'),(180,2,74,'OWNER','2020-06-11 17:34:27','2020-06-11 17:34:27'),(181,1,75,'OWNER','2020-06-24 13:56:52','2020-06-24 13:56:52'),(182,5,76,'OWNER','2020-10-21 10:47:01','2020-10-21 10:47:01'),(183,2,77,'OWNER','2020-12-10 18:16:05','2020-12-10 18:16:05'),(184,26,78,'OWNER','2020-12-10 18:41:22','2020-12-10 18:41:22'),(185,2,79,'OWNER','2020-12-10 19:12:23','2020-12-10 19:12:23');
/*!40000 ALTER TABLE `user_dataset` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-01  5:04:01
