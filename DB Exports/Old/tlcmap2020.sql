-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2019 at 10:00 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tlcmap`
--

-- --------------------------------------------------------

--
-- Table structure for table `dataitem`
--

CREATE TABLE `dataitem` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dataset`
--

CREATE TABLE `dataset` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dataset_dataitem`
--

CREATE TABLE `dataset_dataitem` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dataset_id` int(10) UNSIGNED NOT NULL,
  `dataitem_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dsrole`
--

CREATE TABLE `dsrole` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'REGULAR', 'A regular user, can only browse', NULL, NULL),
(2, 'ADMIN', 'An admin, can browse, add content, edit content, manage users below them', NULL, NULL),
(3, 'SUPER_ADMIN', 'Complete control over entire system and users', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_email` tinytext CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_email`) VALUES
(1, 3, 'c3166457@uon.edu.au'),
(2, 2, 'test@test.test'),
(3, 1, 'inactive@abc.com'),
(4, 1, 'regularuser@abc.com');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `name` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `email_verified_at` date DEFAULT NULL,
  `password` tinytext NOT NULL,
  `remember_token` tinytext DEFAULT NULL,
  `updated_at` date DEFAULT current_timestamp(),
  `created_at` date DEFAULT current_timestamp(),
  `is_active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`name`, `email`, `email_verified_at`, `password`, `remember_token`, `updated_at`, `created_at`, `is_active`) VALUES
('ben', 'c3166457@uon.edu.au', NULL, '$2y$10$XmjT1EXAlHBaXXv5vUtXn.sYssFkeKMelaCqPoR4XEBsWCaHO56vW', 'e9mCtUdykBB9K9LSqMkKYezbCAOTw7xZP09O3JS2lMmE8O2dART7RanyftMo', '2019-11-22', '2019-11-14', 1),
('inactive', 'inactive@abc.com', NULL, '$2y$10$TZKQ3Y9H535prszs7QIsN..TV3MgkM.iOcr./Ahx7DX3AoWlXP9aS', NULL, '2019-11-22', '2019-11-21', 0),
('regularuser', 'regularuser@abc.com', NULL, '$2y$10$6um3EvVKn1UeR4/XpF8yZOADOZcy0QZpcXVGUkX7g4mrSIXOrjIPe', '0HAJE1tOfDnKqo3FiM8aENpCR72SyDbtmBSKVL3qBNQVTAe01TOck2L2R33a', '2019-11-27', '2019-11-27', 1),
('test', 'test@test.test', NULL, '$2y$10$PjmHbM.2Hq6Fgi7qr/huUuXmSjMYqeRya1xX5bEQ79P2qaz6mJqMO', '9MhXcLAM0nWImpBwEYxiKbrnpgDf5QsPOXtDTMZfmrl8JqXOUbl77WqaZwzd', '2019-11-21', '2019-11-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_dataset`
--

CREATE TABLE `user_dataset` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dataset_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_dataset_dsrole`
--

CREATE TABLE `user_dataset_dsrole` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_dataset_id` int(10) UNSIGNED NOT NULL,
  `dsrole_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dataitem`
--
ALTER TABLE `dataitem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dataset`
--
ALTER TABLE `dataset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dataset_dataitem`
--
ALTER TABLE `dataset_dataitem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dsrole`
--
ALTER TABLE `dsrole`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`(255));

--
-- Indexes for table `user_dataset`
--
ALTER TABLE `user_dataset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_dataset_dsrole`
--
ALTER TABLE `user_dataset_dsrole`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dataitem`
--
ALTER TABLE `dataitem`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dataset`
--
ALTER TABLE `dataset`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dataset_dataitem`
--
ALTER TABLE `dataset_dataitem`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dsrole`
--
ALTER TABLE `dsrole`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_dataset`
--
ALTER TABLE `user_dataset`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_dataset_dsrole`
--
ALTER TABLE `user_dataset_dsrole`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
