Wednesday 8th January 2020

11:00 - 13:00			2hrs worked

--------------------------------------------------------
What I did today:
	
	'Edit Collaborators for this Dataset' page
	Partially Created db model for collablink , which stores a random string that another user can enter into the system to become a collaborator on this dataset
		TODO Owner can destroy links, edit/delete collaborators 
	
--------------------------------------------------------
Questions:	

			
--------------------------------------------------------
What I need to do next time:

	dataset rename, description
	add/remove collaborators/viewers
	
	link other users to dataset as viewer/collaborator
		ensure viewers/collaborators can only do actions permitted to their roles
		
	allow for additional data to be inputted for dataitems
	check constraints on inputs (minimum data must be present, no letters in numeric fields, etc)
	
	

--------------------------------------------------------
Notes:

	c3166457@uon.edu.au 		password
	test@test.test@test			testtest
	
	
--------------------------------------------------------
Short Term goals (~ Means partially complete, ~~~ Means fully complete): 

~Fuzzy algorithm --- soundex query on DB --- Run on results the PHP function similar_text (returns a number that is higher if closer)
	A more robust solution using DB servers and index caching might be better, look into it
~Input text file of multiple placenames
	Can take a text file of placenames, system needs to deny wrong filetypes, have more options for input, a textfield to paste in multiple inputs, etc 
~~~js autocomplete on lgas
Chunks on search form
~User accounts via laravel
	~Fix password reset, 
	add roles, 
	etc

--------------------------------------------------------
Overall Goals:

~Finish previous/short term goals

Create account, upload my dataset, public/private, flag as trustworthy/not etc
	Add geodata sets with bare minimums (multiple formats convert into the basic format, etc)
		Solutions to overlapping/conflicting data
			Come up with solutions and present them to Bill
			
Fix slow speed of Joining source data to entries 
	(consider an ajax request when clicking 'view sources' or joining via sql instead of manually afterward somehow)

Big data solutions - Working with such a large DB can be slow, especially when gathering source info or fuzzysearching
	Look into better solutions (A more robust solution using DB servers and index caching might be better)
	
Aurin - grant funded body - mapping (similar) - Check out tools we can use - familiarise

FAQs tlcmap website

Clean the code up a little and document it

Document the system overall

Fix site css a little


