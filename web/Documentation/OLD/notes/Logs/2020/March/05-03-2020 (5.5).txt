Thursday 5th March 2020

Worked: 11:00 - 19:30 (8.5 hrs)
Break: 
		13:15 - 14:15 ( 1 hr )
		16:00 - 18:00 ( 2 hrs )
Total: 5.5hrs

Task List

					-Investigate fuzzy search for dataitems- 			DONE, doesn't sort by closeness
					-Write code for KML to handle dataitems-			DONE
					-Write code for json to handle dataitems-			DONE
					-Write code for csv to handle dataitems-			DONE, still doesnt do sources
					-Feature term, parish on webform-					DONE
					-State in dataitems-								DONE
					Gaz or dataset column on search results?
					Fix download as json button
					Make all download buttons open a dialogue?

	Allow sorting/searching/filtering on search results	
		if using datatables do I need to load ALL results onto the page? (otherwise page will reload each time I enter data)
	
	Check that master/child kml and geoJsons work as intended (especially after the rework!)
	
	See if datasets need speeding up (it's slow on Bills computer/apache)
		
	Fix up dataitems and dataset ajax logic to not use non-unique id - that's what the name field is for! (works but breaks convention)

	
--------------------------------------------------------
What I did today:

	wrote code to handle KML for dataitems - converted old code to a node based format instead of using strings of xml code
	wrote code to handle JSON for dataitems
	wrote code to handle CSV for dataitems
		

--------------------------------------------------------
Last Shift:
	
	search public datasets from main search page, display tickboxes for ausgaz and public datasets, both ticked by default 
		only works for 'name' so far due to mismatched variables
		
	update the html view with @if declarations to show data specific to dataitems
	
	Fixed search query to not show private datasets
	
	Fixed copy link for dataitems in search results
	
	Fixed lga for dataitems in search results
		

--------------------------------------------------------
Known Errors:

	New search doesnt work properly with fuzzyname - probably a pagination issue

	Out of memory error when searching >100k results without pagination (or as kml json csv) - An error in the max size it can handle at once
		Chunking the query (laravel) doesn't fix this, we should paginate and then somehow get each page as a different request and put it into a zip to download or something
		
	Error when attempting to download a kml where the results are large - seems to only get the last (laravel) chunk? 
	
	Date loaded from DB and date displayed in date pickers are displayed differently. Under the hood the format is always yyyy-mm-dd as is the standard, but html date elements will display according to timezone, so formats such as dd/mm/yyyy or mm/dd/yyyy will be displayed. This causes some confusion when searching for dates on a table, as searching 01/02/2020 will display no results, but searching 2020-02-01 will display the result "01/02/2020".
	
--------------------------------------------------------
Notes:

	We can search by exact name match, or by exact + contains + sounds like match (fuzzy), but we do not have a search for just CONTAINS

--------------------------------------------------------
SQL  
		 