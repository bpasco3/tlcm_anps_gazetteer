Wednesday 25th March 2020

Shift:  11:30 - 13:30 (2)
		
Total:  2

Task List

				Fix coordinate wrapping for bbox polygon circle
					not ST_CONTAINS ST_GEOMFROMTEXT POLYGON does not automatically do this (searching from 0-100 shows results at 90, searching from 200-500 does not even though 450 == 90)
					
			
						
				
				
				
				
				Change the search parameters to be more inline with Trove/Google (see the document attached to the task on Jira)
				
				only show max paging error for search form not from url
				
				For KML/JSON/CSV when we reach the MAX_LIMIT warning page, we could re-query with paging=MAX_PAGING to avoid performing the large query again
				EG 
						CURRENT: limit is 50, returns 1 million: Searches for 1 million results -> gets the count -> displays the warning -> searches for 1 mil results -> limits to 50 -> displays the 50
						FUTURE: limit is 50, returns 1 million: Searches for 1 million results -> gets the count -> displays the warning -> searches for first 50 results -> displays the 50
				
					
				Account creation confirmation email - So users cant sign up with someone else's email
				
				Show more info on the search results (whether it came from Gaz or Public Dataset)? Can already infer from whether it has an anps_id or a dataitem_id
				

	
Later:
				Check that master/child kml and geoJsons work as intended (especially after the rework!)
	
				See if datasets need speeding up (it's slow on Bills computer/apache)
		
				Fix up dataitems and dataset ajax logic to not use non-unique id - that's what the name field is for! (works but breaks convention)

	
--------------------------------------------------------
What I did today:

	Merged with Bill's new branch
	Attempted to implement wrapping algorithm
	
	
--------------------------------------------------------
Known Errors:

	latitude range should be -90 to 90 
	longitude range should be -180 to 180
	BUT Rectangle coordinates drawn on the map picker will not loop back around with modulus,
		this means that results such as minlong=0 maxlong=300 are possible when what we want is actually 0 to 180 and then from -180 to -60
			Take this into account with polygon and circle types

	FIXED (ish) (Limits results to an amount set in the .env, displays a warning to the user to alert them that their resultls have been limited)
		Out of memory error when searching >100k results without pagination (or as kml json csv) - An error in the max size it can handle at once
			Chunking the query (laravel) doesn't fix this, we should paginate and then somehow get each page as a different request and put it into a zip to download or something
	
	Date loaded from DB and date displayed in date pickers are displayed differently. Under the hood the format is always yyyy-mm-dd as is the standard, but html date elements will display according to timezone, so formats such as dd/mm/yyyy or mm/dd/yyyy will be displayed. This causes some confusion when searching for dates on a table, as searching 01/02/2020 will display no results, but searching 2020-02-01 will display the result "01/02/2020".
	
--------------------------------------------------------
Notes:

	We can search by exact name match, or by exact + contains + sounds like match (fuzzy), but we do not have a search for just CONTAINS

--------------------------------------------------------
BBOX Wrapping Algorithm

	$dif = $from - $to;
	if ($dif >= 360 || $dif <= -360) *search all results*
	if ($from <= 180 && $from >= -180 && $to <= 180 && $to >= -180) { //if in proper range, ie user input
		if ($from <= $to) *search (longitude >= FROM && longitude <= TO)*
		else *search (longitude >= FROM || longitude <= TO)*
	}
	else { //outside of bounds, we need to wrap
		while ($from > 180) { //put $from into appropriate range, mutate $to appropriately
			$from -= 360;
			$to -= 360;
		}
		while ($from < -180) {
			$from += 360;
			$to += 360;
		}
	}
	if ($to > 180) {
		$x = $to - 360; //340 becomes -20, 360 becomes 0, 720 becoes 360, etc
		*search (longitude >= $from || longitude <= x)*
	}
	//to is now in proper range
	*search (longitude >= FROM && longitude <= TO)*
	
Far more confusing for polygons...
	
	
				



--------------------------------------------------------




SQL  

POLYGON
	SELECT * FROM testplaces WHERE ST_CONTAINS( ST_GEOMFROMTEXT('POLYGON((0 0, 0 100, 100 100, 100 0, 0 0))'), POINT(testplaces.longitude,testplaces.latitude) )
	
CIRCLE
	circle has a centrepoint and a radius, so we just use 
		ST_DISTANCE(POINT(longitude,latitude),centrepoint) <= radius 
		 