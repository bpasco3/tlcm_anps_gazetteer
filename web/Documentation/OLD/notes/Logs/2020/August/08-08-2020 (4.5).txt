14:30 - 19:00 (4.5hrs)

------------------------------------------------------------------------------------------
LOG
- Fixed leaflet map resize on splitter drag.
- Coordinates now wrap around (eg: longitude 190 is actually -170). Map will always return latitude between -90 and 90, longitude between -180 and 180.
- Refactored some functions to avoid duplicated code.

------------------------------------------------------------------------------------------
TODO
1) Distortable Image
2) fix time slider tool (general, cleanup, epoch, importable)
3) button to add time cols to spreadsheet - datepicker on those cols (BC friendly, see gaz one)
4) User Guide
5) Message box on tool with undo button