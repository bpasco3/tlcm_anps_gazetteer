Hours Worked: 5.25
Shift: 10:00 - 17:00 (7)
Break: 13:00 - 14:45 (1.75)

----------------------------------------------------------
What I did:

	Handle datestart,dateend,url in export/import
	BC Dates - convert datestart and dateend to ±YYYYY-MM-DD format
	Investigating account confirmation in laravel
	Meeting
	Account Verification Email
	
----------------------------------------------------------
TODO:


	
----------------------------------------------------------
Notes:

	on public dataitem -> search (displays public data item) -> download as kml/json/csv -> bulk import to dataset from file
		Adds a copy of the data in its current format, not a reference to the public dataitem
		If the same dataitem is added to a dataset more than once it is ignored, does not do so for values from anps register
			eg if I export results from exact match 'Newcastle' (4 from register, 1 from public dataset) as csv kml and json THEN import all 3 into a dataset I will have 13 entries in my dataset as it ignores duplicate dataitems from public dataset
			
	
	
	
	next stable by june 1st