2nd April 2020

Shift:  12:30 - 15:00

	laravel look into authorization authentication (OKTA)
	
	convert lines and polygons to a single centre point - kml add to database should handle this
	
	meetings are now 3pm (AEST)
	
	weekly update email

--------------------------------------------------------
What I did today:

	Fuzzysearch ranking - ranks by closeness to the search term
	Public datasets now show owner name instead of email (privacy issue, if they wish to specify a contact email they can do so in the dataset 'contact' section)
	Meeting with Bill & group
		new images not pushed through on your branch
		ask about add kml handle lines and polygons
	
--------------------------------------------------------
Known Errors:

	latitude range should be -90 to 90 
	longitude range should be -180 to 180
	BUT Rectangle coordinates drawn on the map picker will not loop back around with modulus,
		this means that results such as minlong=0 maxlong=300 are possible when what we want is actually 0 to 180 and then from -180 to -60
			Take this into account with polygon and circle types

	FIXED (ish) (Limits results to an amount set in the .env, displays a warning to the user to alert them that their resultls have been limited)
		Out of memory error when searching >100k results without pagination (or as kml json csv) - An error in the max size it can handle at once
			Chunking the query (laravel) doesn't fix this, we should paginate and then somehow get each page as a different request and put it into a zip to download or something
	
	Date loaded from DB and date displayed in date pickers are displayed differently. Under the hood the format is always yyyy-mm-dd as is the standard, but html date elements will display according to timezone, so formats such as dd/mm/yyyy or mm/dd/yyyy will be displayed. This causes some confusion when searching for dates on a table, as searching 01/02/2020 will display no results, but searching 2020-02-01 will display the result "01/02/2020".
			Dates may be changed to free text to allow for BC dates in future
	
--------------------------------------------------------
Notes:

	We can search by exact name match, or by exact + contains + sounds like match (fuzzy), but we do not have a search for just CONTAINS

--------------------------------------------------------
BBOX Wrapping Algorithm

	$dif = $from - $to;
	if ($dif >= 360 || $dif <= -360) *search all results*
	if ($from <= 180 && $from >= -180 && $to <= 180 && $to >= -180) { //if in proper range, ie user input
		if ($from <= $to) *search (longitude >= FROM && longitude <= TO)*
		else *search (longitude >= FROM || longitude <= TO)*
	}
	else { //outside of bounds, we need to wrap
		while ($from > 180) { //put $from into appropriate range, mutate $to appropriately
			$from -= 360;
			$to -= 360;
		}
		while ($from < -180) {
			$from += 360;
			$to += 360;
		}
	}
	if ($to > 180) {
		$x = $to - 360; //340 becomes -20, 360 becomes 0, 720 becoes 360, etc
		*search (longitude >= $from || longitude <= x)*
	}
	//to is now in proper range
	*search (longitude >= FROM && longitude <= TO)*
	
	Far more confusing for polygons...
	
	
				



--------------------------------------------------------
SQL  

	POLYGON
		SELECT * FROM testplaces WHERE ST_CONTAINS( ST_GEOMFROMTEXT('POLYGON((0 0, 0 100, 100 100, 100 0, 0 0))'), POINT(testplaces.longitude,testplaces.latitude) )
		
	CIRCLE
		circle has a centrepoint and a radius, so we just use 
			ST_DISTANCE(POINT(longitude,latitude),centrepoint) <= radius 
			 