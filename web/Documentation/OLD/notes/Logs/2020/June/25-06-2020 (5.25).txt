Shift : 11:15 - 16:30
Break : 
Worked: 5.25hr


Previous shifts have been additional HuNi attempts, with the latest being on a fresh ubuntu install
	Seems to mostly have worked but there is no data in the DB or it is not loading properly
		Moving on to gazetteer tasks
		
Merge + Logs

Registration page email validator now correctly checks db for existing accounts and displays an appropriate in-line message instead of a stack trace error page.
Made webform map slightly larger.

Emails now come from TLCMap instead of Example.
Logged in user now has their username (character limited to 16chars) visible in the navbar on the Account dropdown button.

Added edit user page, can successfully edit name and password.
Added name and password verification on front and backend (old password must match one in DB, new password and confirm new password must match, etc)

meeting
