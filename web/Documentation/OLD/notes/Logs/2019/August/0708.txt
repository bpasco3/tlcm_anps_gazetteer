Wednesday 7th August
10:00 - 14:00	no Break
	
What I did today
	Discussed previous questions with Dr Bill
	Implemented the query for source info into laravel
	Updated results table view to show source info
	Updated KML to show source info
	Added export as geojson option, seems to be valid (also contains source info)
	Added export to csv option - needs further testing
	Added "download as" buttons to the results page
	Added "download?" button to the search page
	
	
	
Questions:
	data arrays within kml/geojson
	geojson not showing some of my custom data?
	
			
			
What I need to do next time:
	commas for bbox
	
	Expand upon the web form for web users (see jira):
		x results per page (paging)
		Sort by column
		autocomplete for LGA/source
		bbox
		maybe subquery?
		etc
		
	Expand upon the filter options
		source
		split file into x chunks , max filesize of y
		
	Look into options for splitting kml/geojson/csv into smaller files (parent-child or toggleable features)
	
	Ensure my service endpoints (url addresses) properly match the reqs (ie search?)
	
	
	
Future goals:
	Finish Webapp
	Finish Web Services
		
-----------------------------------------------------------------------------------------------------------------------------------------------------------
	
	//sql query to get the distinct sources for each id
	SELECT DISTINCT documentation.anps_id, source.* FROM source INNER JOIN documentation ON source.source_id = documentation.doc_source_id
	
	
	//laravel php code for it
		//Create an associative array that maps an anps_id to an array of source info
        $asoc_array = array();

        //query for all distinct matches between source.source_id and documentation.doc_source_id
        $source_anps_matches = $src->join('documentation','source.source_id','=','documentation.doc_source_id')->select('documentation.anps_id', 'source.*')->distinct(); 
        
        //for each anps_id we have in results,
        foreach ($results as $result) { 
            //Get the sources for that id from the match query
            $sources = $source_anps_matches->where('documentation.anps_id','=',$result->anps_id)->get();
            
            //push it into array for this key
            $asoc_array[$result->anps_id] = $sources;
        }
		
	//push to view
	return view('ws.ghap.places.show')->withDetails($results)->withQuery("test")->with('sources', $asoc_array);
		
		
	//blade read it
	@isset($sources)
		@isset($sources[$line->anps_id])
            @foreach($sources[$line->anps_id] as $source)
                <tr>
                    <td>{{$source->source_id}}</td>
					
					etc...
	