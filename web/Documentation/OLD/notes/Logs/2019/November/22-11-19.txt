Friday 22/11/19

Payroll date: 22/11 - 5/12 inclusive

13:00 - 16:30	

--------------------------------------------------------
What I did today:

	css edits to make it more in line with the TLCMap website https://tlcmap.newcastle.edu.au/
	Defined some rules for users and minimul data for uploading entries to gazetteer C:\Users\bjm662\Documents\TLC\mindata.docx


--------------------------------------------------------
Questions:
	Ask Bill about user roles:
		User roles - 
			Current
				REGULAR - any user who has loaded up the page and registered an account
				ADMIN - A user with admin privileges (what should these be)
				SUPER_ADMIN - Complete control over the system, reserved for Bill, Hugh, etc
				
			Future
				A user who is flagged as being an acedemic of some kind (their uploads are considered trustworthy)
				??
				???
				
		User can be active / inactive (essentially suspending/unsuspending a user's account)
			
--------------------------------------------------------
What I need to do next time:

	Ability to add to register
		Initially require admin access
			Add additional columns (maybe) to say this was added by a user, and which user,etc
		Eventually any registered user can add (flag differently for regular user vs admin)

--------------------------------------------------------
Notes:
	login: c3166457@uon.edu.au 		newpassword
	
--------------------------------------------------------
Short Term goals (~ Means partially complete, ~~~ Means fully complete): 

~Fuzzy algorithm --- soundex query on DB --- Run on results the PHP function similar_text (returns a number that is higher if closer)
	A more robust solution using DB servers and index caching might be better, look into it
~Input text file of multiple placenames
	Can take a text file of placenames, system needs to deny wrong filetypes, have more options for input, a textfield to paste in multiple inputs, etc 
~~~js autocomplete on lgas
Chunks on search form
~User accounts via laravel
	~Fix password reset, 
	add roles, 
	etc

--------------------------------------------------------
Overall Goals:

~Finish previous/short term goals

Create account, upload my dataset, public/private, flag as trustworthy/not etc
	Add geodata sets with bare minimums (multiple formats convert into the basic format, etc)
		Solutions to overlapping/conflicting data
			Come up with solutions and present them to Bill
			
Fix slow speed of Joining source data to entries 
	(consider an ajax request when clicking 'view sources' or joining via sql instead of manually afterward somehow)

Big data solutions - Working with such a large DB can be slow, especially when gathering source info or fuzzysearching
	Look into better solutions (A more robust solution using DB servers and index caching might be better)
	
Aurin - grant funded body - mapping (similar) - Check out tools we can use - familiarise

FAQs tlcmap website

Clean the code up a little and document it

Document the system overall

Fix site css a little


