Thursday 14/11/19

12:15 - 6:00		Break from 12:40 to 1:10 (30 mins) 	Worked 5hrs 15min

--------------------------------------------------------
What I did today:

	Autocomplete for LGA from DB, including supporting css - uses jQuery UI
		Took a while due to difficulties exporting a usable json array without column names + css clashing
		
	Research laravel authenitcation and user model
	
	Implemented user logins!
		Saves to tlcmap DB under users table
			Register, Login, Logout work
				Password reset sends email, token is incorrect (wrong db type? look into it)

--------------------------------------------------------
Questions:


--------------------------------------------------------
What I need to do next time:


--------------------------------------------------------
Notes:

--------------------------------------------------------
Short Term goals: 

~Fuzzy algorithm --- soundex query on DB --- Run on results the PHP function similar_text (returns a number that is higher if closer)
~Input text file of multiple placenames, output their coordinates in 
~js autocomplete on lga
Chunks on search form
~User accounts via laravel
	fix password reset

--------------------------------------------------------
Overall Goals:

Finish previous/short term goals
Allowing people to add their own places (flag as such)
Add geodata sets with bare minimums
Big data solutions
Aurin - grant funded body - mapping (similar) - Check out tools we can use - familiarise
~Other aspect (allowing other people to contribute, login/register system)

Create account, upload my dataset, public/private, flag as trustworthy/not etc
	Solutions to overlapping/conflicting data
		Come up with ideas and present them to Bill

FAQs tlcmap website
