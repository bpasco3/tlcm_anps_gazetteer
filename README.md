# README #

### What is this repository for? ###

* Web interface to search and access ANPS gazetteer of Australian Placenames, and for users to contribute datasets of other placenames.
* The ANPS data is under its licences terms and conditions. Any of our development is MIT licence or Apache 2.0 licence.
* This web interface produced through ARC funded grant for Time Layered Cultural Map, lead by C21CH at University of Newcastle, Australia (Lead CI Prof Hugh Craig).
* System Architect: Bill Pascoe
* Lead developer: Ben McDonnell
* www.tlcmap.org/ghap

### How do I get set up? ###

Laravel application.
Postgres DB with PostGIS

Deployment is in Apache, so put the main code outside of /var/www/htdocs somewhere like /var/www/laravel/ghap
copy the 'public' directory to /var/www/html/ghap/ and correct the paths in index.php, etc.
set permissions correct. For full instructions see the deployments doc (this is internal for TLCMap and UON IT staff).

### ERRORS:
# Failed to authenticate on SMTP server with username "tlcmaptest" using 3 possible authenticators.
Uses google email account 'tlcmaptest' and smtp.gmail.com to send emails for user account management (password reset etc) that is configured in the .env file.
Do not enable 2 step authentication, but you may need to log in to that account, enable insecure app access, and unlock with this:
https://accounts.google.com/DisplayUnlockCaptcha
as describe here:
https://support.google.com/mail/answer/7126229?visit_id=636865986363810933-1186759849&rd=2#cantsignin

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

tlcmap@newcastle.edu.au
* Bill Pascoe, UON
* Ben McDonnell UON