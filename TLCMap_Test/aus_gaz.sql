DROP TABLE IF EXISTS aus_gaz;

CREATE TABLE aus_gaz (
	id INT NOT NULL,
	Record_ID VARCHAR(50),
	Authority VARCHAR(50),
	State_ID VARCHAR(50),
	Name VARCHAR(255),
	Feature_code VARCHAR(50),
	Status VARCHAR(50),
	Postcode VARCHAR(50),
	Concise_gaz VARCHAR(50),
	Longitude VARCHAR(50),
	Long_degrees VARCHAR(50),
	Long_minutes VARCHAR(50),
	Long_seconds VARCHAR(50),
	Latitude VARCHAR(50),
	Lat_degrees VARCHAR(50),
	Lat_minutes VARCHAR(50),
	Lat_seconds VARCHAR(50),
	Map_100K VARCHAR(50),
	CGDN VARCHAR(50),
	Place_ID INT
);

LOAD DATA LOCAL INFILE 'C:/Users/bjm662/Documents/TLC/ANPS/aus_gazetteer.csv'
INTO TABLE aus_gaz
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

CREATE INDEX recordid_index ON aus_gaz(Record_ID);
CREATE INDEX name_index ON aus_gaz(Name);
CREATE INDEX stateid_index ON aus_gaz(State_ID);