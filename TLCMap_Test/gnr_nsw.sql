DROP TABLE IF EXISTS gnr_nsw;

CREATE TABLE gnr_nsw (
	reference INT NOT NULL,
	placename VARCHAR(255) NOT NULL,
	designation VARCHAR(50),
	status VARCHAR(50),
	gazette_date VARCHAR(50),
	geographical_name VARCHAR(255),
	PREVIOUS_NAMES VARCHAR(255),
	GNB_FILE VARCHAR(50),
	LGA VARCHAR(50),
	APPROX_AGD66_LAT VARCHAR(50),
	APPROX_AGD66_LONG VARCHAR(50),
	APPROX_GDA94_LAT VARCHAR(50),
	APPROX_GDA94_LONG VARCHAR(50),
	AGD66_TOPO_MAP VARCHAR(50),
	MAP_NAME VARCHAR(255),
	MAP_NO VARCHAR(50),
	PARISH VARCHAR(50),
	COUNTY VARCHAR(50),
	DESCRIPTION LONGTEXT,
	MEANING LONGTEXT,
	ORIGIN VARCHAR(255),
	HISTORY LONGTEXT
);

LOAD DATA LOCAL INFILE 'C:/Users/bjm662/Documents/TLC/ANPS/gnr_nsw_1.csv'
INTO TABLE gnr_nsw
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 6 ROWS;

LOAD DATA LOCAL INFILE 'C:/Users/bjm662/Documents/TLC/ANPS/gnr_nsw_2.csv'
INTO TABLE gnr_nsw
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 6 ROWS;

CREATE INDEX reference_index ON gnr_nsw(reference);
CREATE INDEX lga_index ON gnr_nsw(LGA);
CREATE INDEX placename_index ON gnr_nsw(placename);