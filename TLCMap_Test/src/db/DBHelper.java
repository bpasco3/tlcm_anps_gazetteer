package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import coordinates.*;
import io.WriteToFile;
import kml.KMLBuilder;
import kml.Placemark;
import text.*;

/*
 * Benjamin McDonnell for TLCMap Project 2019
 * University of Newcastle Australia
 */

public class DBHelper {
	/*
	 * Methods relating to accessing the database for TLCMap
	 */
	final String JDBC_DRIVER;
	final String DB_URL;
	final String USERNAME;
	final String PASSWORD;
	final String TABLE_NAME;
	
	public DBHelper(String jdbc_driver, String db_url, String username, String password, String table_name) {
		JDBC_DRIVER = jdbc_driver;
		DB_URL = db_url;
		USERNAME = username;
		PASSWORD = password;
		TABLE_NAME = table_name;
	}
	
	
	/*
	 * Handles some initial cases where the locational data is present in the anps register description
	 */
	public HashMap<Integer,Coordinates> case1A() {
		/*
		 * Returns a hashmap mapping the anps_id (PK) to the TLCMap Coordinates for that location
		 * Case1A handles the regex SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: %�%\'%" E, %�%\'%" S;%'
		 * Returned Description is of the form Alternative Name: --; Location: 151�51'48" E, 27�42'28" S; QLD Comm...
		 * Different cases exist as the data in table is inconsistently labelled
		 */

		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+ TABLE_NAME +" WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: %�%\\'%\" E, %�%\\'%\" S;%'";

		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.case1A(description);
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case1A ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case1A Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> case1B() {
		/*
		 * As above but for the case of SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___.%, -__%'
		 * cases with decimals in the description
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+ TABLE_NAME +" WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___.%, -__%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.case1B(description);
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case1B ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case1B Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
		
	}

	public HashMap<Integer,Coordinates> case1C() {
		/*
		 * As above but for the case of SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND LIKE '%Location: -__  __  __,  ___  __  __;%'
		 * cases with spaced out degrees/mins
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+ TABLE_NAME +" WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: -__  __  __,  ___  __  __;%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.case1C(description);
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case1C ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case1C Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
		
	}
	
	public HashMap<Integer,Coordinates> case1D() {
		/*
		 * As above but for the case of SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___� %\' %,%'
		 * cases with description with decimal symbol but no minutes
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+ TABLE_NAME +" WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___� %\\' %,%' AND description NOT LIKE '%Phillip Island%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.case1D(description);
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case1D ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case1D Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
		
	}
	
	public HashMap<Integer,Coordinates> case1G() {
		/*
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{2,3}.?[0-9]{1,8}, -[0-9]{2}.?[0-9]{1,8}')
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+ TABLE_NAME +" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{2,3}.?[0-9]{1,10}, -[0-9]{2}.?[0-9]{1,10}')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.case1G(description);
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case1G ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case1G Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
		
	}
	
	
	/*
	 * Handles cases where the anps register already had some info in the lat/long columns
	 */
	public HashMap<Integer,Coordinates> case2A() {
		/*
		 * -32				115.8653
		 * and latitude REGEXP('-[0-9]{2}$')
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,latitude,longitude FROM "+ TABLE_NAME +" WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND latitude REGEXP('-[0-9]{2}$') AND TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Coordinates co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case2A ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case2A Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
		
	}
	
	public HashMap<Integer,Coordinates> case2C() {
		/*
		 * As above but for the case of SELECT anps_id,description REGISTER WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND latitude<>'' AND latitude LIKE '-__.%'
		 * cases with decimals in the description
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,latitude,longitude FROM "+ TABLE_NAME +" WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND latitude<>'' AND latitude LIKE '-__.%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Coordinates co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case1D ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case1D Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
		
	}
	
	/*
	 * Handles information from outside sources that we have previously pulled into our db into separate tables
	 */
	
	public HashMap<Integer,Coordinates> gnr_nsw() {
		/*
		 * Get the incidences where we have matching ids between the register table and the gnr table and the latitude column actually has the latitude in it
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id, APPROX_GDA94_LAT, APPROX_GDA94_LONG FROM gnr_nsw INNER JOIN "+TABLE_NAME+" ON reference=state_id WHERE APPROX_GDA94_LAT LIKE '%-__  __  __%' AND state_code='NSW'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Cases cases = new Cases();
					Coordinates co = cases.gnr_nsw(latitude,longitude);
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the gnr_nsw ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the gnr_nsw Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> gnr_nsw_3() {
		/*
		 * cases where reference and stateid dont match but the placename and LGA do match, which is sufficient enough 
		 * FROM register INNER JOIN gnr_nsw ON (register.placename=gnr_nsw.placename AND register.lga_name=gnr_nsw.LGA) WHERE register.TLCM_Latitude IS NULL AND register.TLCM_GDA94_Latitude_Decimal IS NULL AND register.state_code='NSW' AND APPROX_GDA94_LAT LIKE '%-__  __  __%'
		 * 251 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id, gnr_nsw.APPROX_GDA94_LAT, gnr_nsw.APPROX_GDA94_LONG FROM register INNER JOIN gnr_nsw ON (register.placename=gnr_nsw.placename AND register.lga_name=gnr_nsw.LGA) WHERE register.TLCM_Latitude IS NULL AND register.TLCM_GDA94_Latitude_Decimal IS NULL" + 
				" AND register.state_code='NSW'" + 
				" AND APPROX_GDA94_LAT LIKE '%-__  __  __%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Cases cases = new Cases();
					Coordinates co = cases.gnr_nsw(latitude,longitude);	
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the gnr_nsw_3 ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the gnr_nsw_3 Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> gazetteer() {
		/*
		 * From the Australian Gazetteer
		 * Matching placenames between register and the gnr for tasmania, cutting out duplicate placenames
		 * gnr has Latitude and Longitude columns with already decimalised data
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id,register.state_id,aus_gaz.Record_ID,aus_gaz.Latitude,aus_gaz.Longitude FROM register INNER JOIN aus_gaz ON register.placename=aus_gaz.Name WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND register.state_code=aus_gaz.State_ID AND register.state_id IS NOT NULL";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String registerID = rs.getString(2);
					String gnrID = rs.getString(3);
					String latitude = rs.getString(4);
					String longitude = rs.getString(5);
					Cases cases = new Cases();
					Coordinates co = cases.gnr_tas(registerID,gnrID,latitude,longitude); 
					if (co != null) hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the gazetteer ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the gazetteer Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> lga_tas() {
		/*
		 * From the Tasmanian Listmap download of local government areas
		 * http://listdata.thelist.tas.gov.au/opendata/
		 * Used QGIS to output the centroid coordinates for each polygon
		 * For cases where we cannot get any locational data for these towns but they have an LGA that matches to one present here
		 * We will simply put the LGA centre point coordinates for them for now
		 * We will flag these as 'Only accurate to LGA'
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT r.anps_id, t.Latitude, t.Longitude from register r INNER JOIN lga_tas t ON r.lga_name=t.NAME WHERE r.state_code='TAS' AND r.TLCM_Latitude IS NULL AND r.TLCM_GDA94_Latitude_Decimal IS NULL GROUP BY anps_id";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Coordinates co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the lga_tas ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the lga_tas Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> gnr_vic() {
		/*
		 * Uses the gnr_vic table
		 * https://maps.land.vic.gov.au/lassi/VicnamesUI.jsp			downloaded as csv, imported to db using sql
		 * SELECT * FROM register INNER JOIN gnr_vic ON state_id=gnr_vic.placeid WHERE register.state_code='VIC' AND gnr_vic.longitude != "" AND gnr_vic.latitude != "" AND TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL GROUP BY anps_id
		 * 47 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id,gnr_vic.latitude,gnr_vic.longitude FROM register INNER JOIN gnr_vic ON state_id=gnr_vic.placeid WHERE register.state_code='VIC' AND gnr_vic.longitude != \"\" AND gnr_vic.latitude != \"\" AND TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL GROUP BY anps_id";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Coordinates co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the gnr_vic ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the gnr_vic Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	
	
	
	/*
	 * Handles a few extra cases with location in description
	 */
	
	public HashMap<Integer,Coordinates> case4A() {
		/*
		 * Description contains locational data but in odd format
		 * Postcode: 7140; Long.: 152.606674194335; Lat.: -26.9663887023925; 100K Map: 9444
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND state_code = 'QLD' AND description REGEXP('Long.:')
		 * 1782 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id, description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND state_code = 'QLD' AND description REGEXP('Long.:')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.case4A(description);	
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case4A ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case4A Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> case4E() {
		/*
		 * Description contains locational data but in odd format
		 *	AND description LIKE '%�%'
		 *	LATITUDE: 33� 59' S \nLONGITUDE: 122� 13' E
		 *	19 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id, description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description LIKE '%�%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.case4E(description);	
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the Case4E ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Case4E Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	
	/*
	 * Handles some odd outlier cases
	 */
	public HashMap<Integer,Coordinates> caseOA() {
		/*
		 * description contains 'official; 145.202222222222, -37.8361111111111'
		 * 						 official; 142.40125, -34.5173611111111
		 * 						 official; 142.068194444444, -38.2562499999999
		 * 						 historical; 146.541805555555, -36.7731944444444
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('; [0-9]{3}.[0-9]{1,15}, -[0-9]{2}.[0-9]{1,15}')
		 * 298 results
		 */
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('; [0-9]{3}.[0-9]{1,15}, -[0-9]{2}.[0-9]{1,15}')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.caseOA(description);	
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the CaseOA ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the CaseOA Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> caseOB() {
		/*
		 * lat and long in the lat/long columns
		 * lat: 31� 31' 15.1" S				'space S' is optional
		 * long: 159� 04' 27.4" E
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND latitude REGEXP('[0-9]{2}� [0-9]{2}\' [0-9]{2}.?[0-9]{0,1}\"[[:space:]]?S?')
		 * 156 results
		 */
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,latitude,longitude FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND latitude REGEXP('[0-9]{2}� [0-9]{2}\\' [0-9]{2}.?[0-9]{0,1}\\\"[[:space:]]?S?')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Cases cases = new Cases();
					Coordinates co = cases.caseOB(latitude,longitude);	
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the CaseOB ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the CaseOB Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> caseOC() {
		/*
		 * description of the form
		 * Status: Official; Other Names: -; Location:149� 7' 0 / -35� 17' 40
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location:[0-9]{3}� [0-9]{1,2}\' [0-9]{1,2} / -[0-9]{2}� [0-9]{1,2}\' [0-9]{1,2}')
		 * 864 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location:[0-9]{3}� [0-9]{1,2}\\' [0-9]{1,2} / -[0-9]{2}� [0-9]{1,2}\\' [0-9]{1,2}')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.caseOC(description);	//SHOULD BE THE SAME AS 3A EXCEPT WE ARE SWAPPING LAT AND LONG
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the CaseOC ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the CaseOC Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates>	caseOD() {
		/*
		 * description of form
		 * Status: Official; Other Names: -; Location:133� 53' 0, -22� 12' 0
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location:[0-9]{3}� [0-9]{1,2}\' [0-9]{1,2}, -[0-9]{2}� [0-9]{1,2}\' [0-9]{1,2}$')
		 * 12,604 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location:[0-9]{3}� [0-9]{1,2}\\' [0-9]{1,2}, -[0-9]{2}� [0-9]{1,2}\\' [0-9]{1,2}$')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.caseOD(description);	//SHOULD BE THE SAME AS 3A EXCEPT WE ARE SWAPPING LAT AND LONG
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the CaseOD ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the CaseOD Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates>	caseOE() {
		/*
		 * description of form
		 * Status: Official; Other Names: --; Location: 150� 37 59, -35� 10 0
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{3}� [0-9]{1,2} [0-9]{1,2}, -[0-9]{2}� [0-9]{1,2} [0-9]{1,2}')
		 * 42 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{3}� [0-9]{1,2} [0-9]{1,2}, -[0-9]{2}� [0-9]{1,2} [0-9]{1,2}')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.caseOE(description);	//SHOULD BE THE SAME AS 3A EXCEPT WE ARE SWAPPING LAT AND LONG
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the CaseOE ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the CaseOE Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> caseOF() {
		/*
		 * description of form
		 * Current Name: MORETON BAY; Location: 153�15'34" E, 27�17'24" S.			optional fullstop
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{3}�[0-9]{1,2}\'[0-9]{1,2}\" E, [0-9]{2}�[0-9]{1,2}\'[0-9]{1,2}\" S.?')
		 * 3 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{3}�[0-9]{1,2}\\'[0-9]{1,2}\\\" E, [0-9]{2}�[0-9]{1,2}\\'[0-9]{1,2}\\\" S.?')";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.caseOF(description);	//SHOULD BE THE SAME AS 3A EXCEPT WE ARE SWAPPING LAT AND LONG
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the CaseOF ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the CaseOF Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> caseOG() {
		/*
		 * description of form
		 * variant of Phillip Island, location: 167� 57' 4, -29� 7' 13
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description like 'variant of Phillip Island, location: 167� 57\' 4, -29� 7\' 13'
		 * 1 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM "+TABLE_NAME+" WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description like 'variant of Phillip Island, location: 167� 57\\' 4, -29� 7\\' 13'";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					Coordinates co = cases.caseOG(description);	//SHOULD BE THE SAME AS 3A EXCEPT WE ARE SWAPPING LAT AND LONG
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the CaseOG ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the CaseOG Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	
	
	/*
	 * Handles matching to the related_names table
	 */
	
	public HashMap<Integer,Coordinates> related_names_1A() {
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id,related_names.placename_anps_id FROM `register` INNER JOIN related_names ON register.anps_id=related_names.related_name_anps_id WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					int related_id = rs.getInt(2);
					
					String sql2 = "SELECT TLCM_Latitude,TLCM_Longitude FROM register WHERE anps_id="+related_id;
					String tlat = "";
					String tlong = "";
					
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						if (rs2.next()) {
							tlat = rs2.getString(1);
							tlong = rs2.getString(2);
						}
					} catch (Exception e) {System.err.println("Error in the related_names_1A resultset 2 Try block"); e.printStackTrace(); }
					if (tlat != null && tlong != null) {
						Coordinates co = new Coordinates(Double.parseDouble(tlat),Double.parseDouble(tlong));
						hm.put(anps_id, co); //put into container
					}
				}
			} catch (Exception e) {System.err.println("Error in the related_names_1A ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the related_names_1A Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> related_names_1B() {
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id,related_names.placename_anps_id FROM `register` INNER JOIN related_names ON register.anps_id=related_names.related_name_anps_id WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					int related_id = rs.getInt(2);
					
					String sql2 = "SELECT TLCM_GDA94_Latitude_Decimal,TLCM_GDA94_Longitude_Decimal FROM register WHERE anps_id="+related_id;
					String tlat = "";
					String tlong = "";
					
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						if (rs2.next()) {
							tlat = rs2.getString(1);
							tlong = rs2.getString(2);
						}
					} catch (Exception e) {System.err.println("Error in the related_names_1B ResultSet2 Try block"); e.printStackTrace(); }
					if (tlat != null && tlong != null) {
						Coordinates co = new Coordinates(Double.parseDouble(tlat),Double.parseDouble(tlong));
						hm.put(anps_id, co); //put into container
					}
				}
			} catch (Exception e) {System.err.println("Error in the related_names_1B ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the related_names_1B Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> related_names_2A() {
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id,related_names.related_name_anps_id FROM `register` INNER JOIN related_names ON register.anps_id=related_names.placename_anps_id WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					int related_id = rs.getInt(2);
					
					String sql2 = "SELECT TLCM_Latitude,TLCM_Longitude FROM register WHERE anps_id="+related_id;
					String tlat = "";
					String tlong = "";
					
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						if (rs2.next()) {
							tlat = rs2.getString(1);
							tlong = rs2.getString(2);
						}
					} catch (Exception e) {System.err.println("Error in the related_names_2A resultset 2 Try block"); e.printStackTrace(); }
					if (tlat != null && tlong != null) {
						Coordinates co = new Coordinates(Double.parseDouble(tlat),Double.parseDouble(tlong));
						hm.put(anps_id, co); //put into container
					}
				}
			} catch (Exception e) {System.err.println("Error in the related_names_2A ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the related_names_2A Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> related_names_2B() {
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id,related_names.related_name_anps_id FROM `register` INNER JOIN related_names ON register.anps_id=related_names.placename_anps_id WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					int related_id = rs.getInt(2);
					
					String sql2 = "SELECT TLCM_GDA94_Latitude_Decimal,TLCM_GDA94_Longitude_Decimal FROM register WHERE anps_id="+related_id;
					String tlat = "";
					String tlong = "";
					
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						if (rs2.next()) {
							tlat = rs2.getString(1);
							tlong = rs2.getString(2);
						}
					} catch (Exception e) {System.err.println("Error in the related_names_2B resultset 2 Try block"); e.printStackTrace(); }
					if (tlat != null && tlong != null) {
						Coordinates co = new Coordinates(Double.parseDouble(tlat),Double.parseDouble(tlong));
						hm.put(anps_id, co); //put into container
					}
				}
			} catch (Exception e) {System.err.println("Error in the related_names_2B ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the related_names_2B Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> stateIdA() {
		/*
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('\\(State ID [0-9]{1,5}\\)') AND description NOT LIKE '%(State Id %)%(State Id %)%'
		 * 65 results
		 * ignores a  few cases where it specifies multiple state ids
		 * A is where we have them in the TLCM_Lat/long 
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description,state_code FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('\\\\(State ID [0-9]{1,5}\\\\)') AND description NOT LIKE '%(State Id %)%(State Id %)%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					String state_code = rs.getString(3);
					Cases cases = new Cases();
					String other_state_id = cases.stateId(description);	//gets the alterante state id from the description
					String sql2 = "SELECT TLCM_Latitude,TLCM_Longitude FROM register WHERE state_id='"+other_state_id+"' AND state_code='"+state_code+"';";
					Coordinates co = null;
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						while (rs2.next()) {
							String latitude = rs2.getString(1);
							String longitude = rs2.getString(2);

							if (latitude != null && longitude != null) {
								co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
							}
						}
					}
					if (co != null) {hm.put(anps_id, co); }//put into container
				}
			} catch (Exception e) {System.err.println("Error in the stateIdA ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the stateIdA Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> stateIdB() {
		/*
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('\\(State ID [0-9]{1,5}\\)') AND description NOT LIKE '%(State Id %)%(State Id %)%'
		 * 65 results
		 * ignores a  few cases where it specifies multiple state ids
		 * B is the cases where we have the GDA coordinates
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description,state_code FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('\\\\(State ID [0-9]{1,5}\\\\)') AND description NOT LIKE '%(State Id %)%(State Id %)%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					String state_code = rs.getString(3);
					Cases cases = new Cases();
					String other_state_id = cases.stateId(description);	//gets the alterante state id from the description
					String sql2 = "SELECT TLCM_GDA94_Latitude_Decimal, TLCM_GDA94_Longitude_Decimal FROM register WHERE state_id='"+other_state_id+"' AND state_code='"+state_code+"';";
					Coordinates co = null;
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						while (rs2.next()) {
							String latitude = rs2.getString(1);
							String longitude = rs2.getString(2);

							if (latitude != null && longitude != null) {
								co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
							}
						}
					}
					if (co != null) {hm.put(anps_id, co); }//put into container
				}
			} catch (Exception e) {System.err.println("Error in the stateIdB ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the stateIdB Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> anpsIdA() {
		/*
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description LIKE '%anps id%'
		 * 28 results
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description LIKE '%anps id%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					String other_anps_id = cases.anpsId(description);	//gets the alterante state id from the description
					String sql2 = "SELECT TLCM_Latitude,TLCM_Longitude FROM register WHERE anps_id="+other_anps_id+";";
					Coordinates co = null;
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						while (rs2.next()) {
							String latitude = rs2.getString(1);
							String longitude = rs2.getString(2);

							if (latitude != null && longitude != null) {
								co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
							}
						}
					}
					if (co != null) {hm.put(anps_id, co); }//put into container
				}
			} catch (Exception e) {System.err.println("Error in the anpsIdA ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the anpsIdA Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	public HashMap<Integer,Coordinates> anpsIdB() {
		/*
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id,description FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description LIKE '%anps id%'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String description = rs.getString(2);
					Cases cases = new Cases();
					String other_anps_id = cases.anpsId(description);	//gets the alterante anps id from the description
					String sql2 = "SELECT TLCM_GDA94_Latitude_Decimal, TLCM_GDA94_Longitude_Decimal FROM register WHERE anps_id="+other_anps_id+";";
					Coordinates co = null;
					try (ResultSet rs2 = stmt.executeQuery(sql2);) {
						while (rs2.next()) {
							String latitude = rs2.getString(1);
							String longitude = rs2.getString(2);

							if (latitude != null && longitude != null) {
								co = new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
							}
						}
					}
					if (co != null) {hm.put(anps_id, co); }//put into container
				}
			} catch (Exception e) {System.err.println("Error in the anpsIdB ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the anpsIdB Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	/*
	 * 	Cleaning / setting up
	 */
	
	public boolean addTLCMCoordinatesBatch(HashMap<Integer,Coordinates> hm,String lat_column,String long_column, String source) {
		/*
		 * Psuedo Overload
		 */
		return addTLCMCoordinatesBatch(hm,lat_column,long_column,source,null);
	}
	
	public boolean addTLCMCoordinatesBatch(HashMap<Integer,Coordinates> hm,String lat_column,String long_column, String source, String flag) {
		boolean success = false;
		PreparedStatement ps = null;
		Connection conn = null;
		String sql = "";
		
		try {
			if (flag != null) { sql = "UPDATE " + TABLE_NAME + " SET "+lat_column+"=?, "+long_column+"=?, PRIMARY_DATA_SOURCE=IFNULL(PRIMARY_DATA_SOURCE,'"+source+"'), flag='"+flag+"' WHERE anps_id=?;"; }
			else { sql = "UPDATE " + TABLE_NAME + " SET "+lat_column+"=?, "+long_column+"=?, PRIMARY_DATA_SOURCE=IFNULL(PRIMARY_DATA_SOURCE,'"+source+"'), flag=NULL WHERE anps_id=?;";}
			
			conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			ps = conn.prepareStatement(sql);
			for (int id : hm.keySet()) {
				ps.setString(1, Double.toString(hm.get(id).getLatitude()));
				ps.setString(2, Double.toString(hm.get(id).getLongitude()));
				ps.setInt(3, id);
				
				ps.addBatch();
			}
			
			System.out.println("\nExecuting Update Batch, This may take a few minutes, Please do not exit the program...");
			int[] batches = ps.executeBatch();
			success = true;
			System.out.println("Successfully completed " + batches.length + " updates!");
			
		} catch (Exception e) { System.err.println("Error in the addTLCMCoordinatesBatch Connection/Statement Try block"); e.printStackTrace(); }
		finally {
			if (ps != null) { try {ps.close();} catch (SQLException e) {e.printStackTrace();}}
			if (conn != null) { try {conn.close();} catch(SQLException e) {e.printStackTrace();} }
		}
		return success;
		
	}
	
	public boolean addCoordinateColumns() {
		/*
		 * A function used to add our new columns to the table
		 */
		//Return value, false unless we have successfully completed the SQL query
		boolean success = false;
		
		//SQL - Add columnsto table
		String sql = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN TLCM_Latitude varchar(50) DEFAULT NULL";
		String sql2 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN TLCM_Longitude varchar(50) DEFAULT NULL";
		String sql3 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN GNR_AGD66_Latitude varchar(50) DEFAULT NULL";
		String sql4 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN GNR_AGD66_Longitude varchar(50) DEFAULT NULL";
		String sql5 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN GNR_GDA94_Latitude varchar(50) DEFAULT NULL";
		String sql6 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN GNR_GDA94_Longitude varchar(50) DEFAULT NULL";
		String sql7 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN TLCM_GDA94_Latitude_Decimal varchar(50) DEFAULT NULL";
		String sql8 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN TLCM_GDA94_Longitude_Decimal varchar(50) DEFAULT NULL";
		String sql9 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN PRIMARY_DATA_SOURCE varchar(100) DEFAULT NULL";
		String sql10 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN flag varchar(100) DEFAULT NULL";
		String sql11 = "CREATE INDEX placename_index ON register(placename);"; //for our join later, speeds it up
		String sql12 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN google_maps_link TEXT";
		String sql13 = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN ORIGINAL_DATA_SOURCE varchar(100) DEFAULT NULL";

		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {

			//Report success
			System.out.println("Added the TLCM Coordinates Columns");
			
			stmt.executeUpdate(sql);
			stmt.executeUpdate(sql2);
			stmt.executeUpdate(sql7); //we want these first ideally
			stmt.executeUpdate(sql8); //
			stmt.executeUpdate(sql3);
			stmt.executeUpdate(sql4);
			stmt.executeUpdate(sql5);
			stmt.executeUpdate(sql6);
			stmt.executeUpdate(sql9);
			stmt.executeUpdate(sql10);
			stmt.executeUpdate(sql11);
			stmt.executeUpdate(sql12);
			stmt.executeUpdate(sql13);
			
			success = true;
			
		} catch (Exception e) { System.err.println("Error in the addCoordinateColumns Connection/Statement Try block"); e.printStackTrace(); }

		return success;
	}
	
	public boolean dropCoordinateColumns() {
		/*
		 * Used to return the table to its prior state via code so we dont have to keep manually resetting the table
		 */
		boolean success = false; //return value
		
		//SQL
		String sql = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS TLCM_Latitude";
		String sql2 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS TLCM_Longitude";
		String sql3 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS GNR_AGD66_Latitude";
		String sql4 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS GNR_AGD66_Longitude";
		String sql5 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS GNR_GDA94_Latitude";
		String sql6 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS GNR_GDA94_Longitude";
		String sql7 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS TLCM_GDA94_Latitude_Decimal";
		String sql8 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS TLCM_GDA94_Longitude_Decimal";
		String sql9 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS PRIMARY_DATA_SOURCE";
		String sql10 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS flag";
		String sql11 = "ALTER TABLE " + TABLE_NAME + " DROP INDEX IF EXISTS placename_index";
		String sql12 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS google_maps_link";
		String sql13 = "ALTER TABLE " + TABLE_NAME + " DROP COLUMN IF EXISTS ORIGINAL_DATA_SOURCE";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {

			//Report success
			System.out.println("Cleaning the table...");
			
			stmt.executeUpdate(sql);
			stmt.executeUpdate(sql2);
			stmt.executeUpdate(sql3);
			stmt.executeUpdate(sql4);
			stmt.executeUpdate(sql5);
			stmt.executeUpdate(sql6);
			stmt.executeUpdate(sql7);
			stmt.executeUpdate(sql8);
			stmt.executeUpdate(sql9);
			stmt.executeUpdate(sql10);
			stmt.executeUpdate(sql11);
			stmt.executeUpdate(sql12);
			stmt.executeUpdate(sql13);
			
			success = true;
			
		} catch (Exception e) { System.err.println("Error in the dropCoordinateColumns Connection/Statement Try block"); e.printStackTrace(); }

		return success;
	}
	
	public void copyGNR() {
		/*
		 * Copy the variables over from the GNR table
		 */
		
		String sql = "UPDATE register r, gnr_nsw g SET r.GNR_AGD66_Latitude = g.APPROX_AGD66_LAT, r.GNR_AGD66_Longitude = g.APPROX_AGD66_LONG, r.GNR_GDA94_Latitude = g.APPROX_GDA94_LAT, r.GNR_GDA94_Longitude = g.APPROX_GDA94_LONG WHERE r.state_code='NSW' AND r.state_id regexp('^[0-9]*$') AND r.state_id = g.reference;";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			System.out.println("\nCopying variables over from the gnr table...");
			stmt.executeUpdate(sql);

		} catch (Exception e) { System.err.println("Error in the copyGNR Connection/Statement Try block"); e.printStackTrace(); }
	}
	
	public void insertGoogleLinks() {
		/*
		 * Insert links to the google maps pages
		 */
		String sql = "UPDATE register SET google_maps_link=CONCAT('https://www.google.com/maps/@',TLCM_Latitude,',',TLCM_Longitude,',','15z') WHERE TLCM_Latitude IS NOT NULL";
		String sql2 = "UPDATE register SET google_maps_link=CONCAT('https://www.google.com/maps/@',TLCM_GDA94_Latitude_Decimal,',',TLCM_GDA94_Longitude_Decimal,',','15z') WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NOT NULL";
	
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			System.out.println("\nInserting Google Maps links...");
			stmt.executeUpdate(sql);
			stmt.executeUpdate(sql2);

		} catch (Exception e) { System.err.println("Error in the insertGoogleLinks Connection/Statement Try block"); e.printStackTrace(); }

	}
	
	
	public void flagPlaceholderData() {
		/*
		 * anps has given us 230 rows with -36,135 in the lat/long columns as placeholder data for uncertain locations, FLAG THEM
		 */
		String sql = "UPDATE register SET flag='Placeholder coordinates for uncertain location in anps data in lat/long columns' WHERE latitude='-36' AND longitude='135'";
	
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			System.out.println("\nFlagging uncertain anps data...");
			stmt.executeUpdate(sql);

		} catch (Exception e) { System.err.println("Error in the flagPlaceholderData Connection/Statement Try block"); e.printStackTrace(); }

	}
	
	public void copyOverGDA() {
		/*
		 * We now want all the TLCM_GDA data over into the TLCM_Latitude/TLCM_Longitude columns as they have been flagged as of a different coordinate system
		 */
		String sql = "UPDATE register SET TLCM_Latitude=TLCM_GDA94_Latitude_Decimal,TLCM_Longitude=TLCM_GDA94_Longitude_Decimal WHERE TLCM_Latitude IS NULL AND TLCM_Longitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NOT NULL AND TLCM_GDA94_Longitude_Decimal IS NOT NULL";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			System.out.println("\nCopying the TLCM_GDA94 column to the TLCM column where appropriate...");
			stmt.executeUpdate(sql);

		} catch (Exception e) { System.err.println("Error in the copyOverGDA Connection/Statement Try block"); e.printStackTrace(); }
	}
	
	public void insertOriginalDataSource() {
		/*
		 * Inserting information into the ORIGINAL_DATA_SOURCE column
		 */
		String sql = "UPDATE register r INNER JOIN documentation d ON r.anps_id = d.anps_id SET r.ORIGINAL_DATA_SOURCE = 'ANPS Research' WHERE r.PRIMARY_DATA_SOURCE='anps latitude and longitude columns.' OR r.PRIMARY_DATA_SOURCE='anps description section.'";
		String sql1 = "UPDATE register r SET r.ORIGINAL_DATA_SOURCE = 'Australian Gazetteer' WHERE r.PRIMARY_DATA_SOURCE='Australian Gazetteer'";
		String sql2 = "UPDATE register r SET r.ORIGINAL_DATA_SOURCE = 'State Records (ANPS)' WHERE (r.PRIMARY_DATA_SOURCE='anps latitude and longitude columns.' OR r.PRIMARY_DATA_SOURCE='anps description section.') AND r.ORIGINAL_DATA_SOURCE IS NULL";
		String sql3 = "UPDATE register r SET r.ORIGINAL_DATA_SOURCE = 'State Records (TLCM)' WHERE (r.PRIMARY_DATA_SOURCE='NSW gnr' OR r.PRIMARY_DATA_SOURCE='VIC gnr') AND r.ORIGINAL_DATA_SOURCE IS NULL";
		String sql4 = "UPDATE register SET ORIGINAL_DATA_SOURCE = 'Variant' WHERE (PRIMARY_DATA_SOURCE LIKE 'Description%' OR PRIMARY_DATA_SOURCE like 'Related%') AND ORIGINAL_DATA_SOURCE IS NULL";
		String sql5 = "UPDATE register SET ORIGINAL_DATA_SOURCE = 'TAS LGA' WHERE (PRIMARY_DATA_SOURCE LIKE 'LGA%') AND ORIGINAL_DATA_SOURCE IS NULL";
		String sql6 = "UPDATE register SET ORIGINAL_DATA_SOURCE = 'NSW LGA' WHERE (PRIMARY_DATA_SOURCE LIKE 'NSW gnr by matching%') AND ORIGINAL_DATA_SOURCE IS NULL";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			System.out.println("\nUpdating the ORIGINAL_DATA_SOURCE column...");
			stmt.executeUpdate(sql);
			stmt.executeUpdate(sql1);
			stmt.executeUpdate(sql2);
			stmt.executeUpdate(sql3);
			stmt.executeUpdate(sql4);
			stmt.executeUpdate(sql5);
			stmt.executeUpdate(sql6);	
		} catch (Exception e) { System.err.println("Error in the insertOriginalDataSource Connection/Statement Try block"); e.printStackTrace(); }
	}
	
	public void buildKML() {
		KMLBuilder kmlb = new KMLBuilder();
		ArrayList<Placemark> placemarks = new ArrayList<>();
		
		//query for input data
		String sql = "SELECT r.anps_id, r.placename, r.state_id, r.description, r.state_code, r.TLCM_Latitude, r.TLCM_Longitude, r.flag, r.google_maps_link, r.ORIGINAL_DATA_SOURCE, "
				+ "IF(s.source_id IS NOT NULL,s.source_id,NULL) as anps_source_id, IF(s.source_id IS NOT NULL,s.source_type,NULL) as anps_source_type, IF(s.source_id IS NOT NULL,s.title,NULL) as anps_source_title, "
				+ "IF(s.source_id IS NOT NULL,s.author,NULL) as anps_source_author, IF(s.source_id IS NOT NULL,s.isbn,NULL) as anps_source_isbn, "
				+ "IF(s.source_id IS NOT NULL,s.publisher,NULL) as anps_source_publisher, IF(s.source_id IS NOT NULL,s.source_place,NULL) as anps_source_place, "
				+ "IF(s.source_id IS NOT NULL,s.source_date,NULL) as anps_source_date, IF(s.source_id IS NOT NULL,s.source_location,NULL) as anps_source_location "
				+ "FROM register r LEFT JOIN (documentation d INNER JOIN source s ON d.doc_source_id=s.source_id) ON r.anps_id=d.anps_id WHERE TLCM_Latitude IS NOT NULL AND state_code='NSW'";
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			System.out.println("\nBuilding the KML...");
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					placemarks.add(new Placemark(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
							rs.getString(5),rs.getDouble(6),rs.getDouble(7),rs.getString(8),rs.getString(9),
							rs.getString(10),rs.getInt(11),rs.getString(12),rs.getString(13),rs.getString(14),
							rs.getString(15),rs.getString(16),rs.getString(17),rs.getString(18),rs.getString(19)));
				}
			} catch (Exception e) {System.err.println("Error in the buildKML ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the buildKML Connection/Statement Try block"); e.printStackTrace(); }
		
		//send it to KMLBuilder as an ArrayList<String>
		WriteToFile wtf = new WriteToFile();
		wtf.requestOutputFilename();
		wtf.write(kmlb.build(placemarks));
	}

	
	
	
	/*********************************
	 *   DEBUG OR OUTDATED METHODS   *
	 *********************************/
	
	
	
	public ArrayList<String> getTable() { 
		/*
		 * Simple pseudo-overload for a cleaner print of the whole table
		 * Potentially a very long wait - Table has >300,000 rows
		 */
		return getTableMax(0); //0 gets all rows
	}
	
	public ArrayList<String> getTableMax(int maxRows) {
		/*
		 * Get this many rows off the top of the table, ordered by the PK Descending
		 * if 0 get all (or upper limit in this case)
		 */
		int UPPER_LIMIT = 10000; //PRINTING ENTIRE TABLE WILL POSSIBLY CRASH THE PROGRAM, ONLY PRINT 10000
		
		//maxRows has an upper limit
		if (maxRows > UPPER_LIMIT || maxRows == 0) maxRows = UPPER_LIMIT;
		
		//Container and return value
		ArrayList<String> rows = new ArrayList<>();
		
		//SQL - //Select data from the table
		String sql = "SELECT * FROM " + TABLE_NAME + " limit " + maxRows + ";";

		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				
				//get number of columns
				ResultSetMetaData rsmd = rs.getMetaData();
				int numColumns = rsmd.getColumnCount();
				System.out.println("Column Count: " + numColumns+ "\n");
				
				boolean first = true;
				while (rs.next()) {
					String line = "";
					//Column names
					if (first) {
						for (int i=1; i <= numColumns; i++) { //print all column names
							if (i!=2 && i!= 8) { line += String.format("%-10.10s  ",rsmd.getColumnName(i)); } //for most columns 15 is wide enough
							else if (i==8){ line += String.format("%-350.350s  ",rsmd.getColumnName(i)); } //for description we want 150 wide
							else { line += String.format("%-25.25s  ",rsmd.getColumnName(i)); } //for place name we want 25 wide
						}
						rows.add(line);
						rows.add("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
						first = false; //dont do this again
					}
					//Get the row for this rs.next()
					line = "";
					for (int i=1; i <= numColumns; i++) {
						if (i!=2 && i!= 8) { line += String.format("%-10.10s  ",rs.getString(i)); }
						else if (i==8){ line += String.format("%-350.350s  ",rs.getString(i)); }
						else { line += String.format("%-25.25s  ",rs.getString(i));  }
					}
					rows.add(line);	
					//Try with resources will automatically close rs, stmt, and connection
				}
			} catch (Exception e) {System.err.println("Error in the ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Connection/Statement Try block"); e.printStackTrace(); }
		return rows;
	}
	
	//encompassed by aus gazetteer
	public HashMap<Integer,Coordinates> gnr_tas() {
		/*
		 * From the GNR for TAS
		 * Matching placenames between register and the gnr for tasmania, cutting out duplicate placenames
		 * gnr has Latitude and Longitude columns with already decimalised data
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT register.anps_id,register.state_id,gnr_tas.Record_ID,gnr_tas.Latitude,gnr_tas.Longitude FROM register,gnr_tas WHERE register.placename = gnr_tas.Name AND register.state_code='TAS' AND register.TLCM_Latitude IS NULL AND register.TLCM_GDA94_Latitude_Decimal IS NULL";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String registerID = rs.getString(2);
					String gnrID = rs.getString(3);
					String latitude = rs.getString(4);
					String longitude = rs.getString(5);
					Cases cases = new Cases();
					Coordinates co = cases.gnr_tas(registerID,gnrID,latitude,longitude); 
					if (co != null) hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the gnr_tas ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the gnr_tas Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	
	//NOT NEEDED?
	public HashMap<Integer,Coordinates> gnr_nsw_2() {
		/*
		 * Get the incidences where we have matching ids between the register table and the gnr table BUT the latitude and longitude are the opposite way around
		 */
		
		HashMap<Integer,Coordinates> hm = new HashMap<>(); //HashMap Container
		
		String sql = "SELECT anps_id, APPROX_GDA94_LAT, APPROX_GDA94_LONG FROM gnr_nsw INNER JOIN "+TABLE_NAME+" ON reference=state_id WHERE APPROX_GDA94_LONG LIKE '%-__  __  __%' AND APPROX_GDA94_LAT LIKE '%___ __ __%' AND state_code='NSW'";
		
		//Attempt Connection and SQL
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while (rs.next()) {
					int anps_id = rs.getInt(1); 
					String latitude = rs.getString(2);
					String longitude = rs.getString(3);
					Cases cases = new Cases();
					Coordinates co = cases.gnr_nsw(longitude,latitude);	//SHOULD BE THE SAME AS 3A EXCEPT WE ARE SWAPPING LAT AND LONG
					hm.put(anps_id, co); //put into container
				}
			} catch (Exception e) {System.err.println("Error in the gnr_nsw_2 ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the gnr_nsw_2 Connection/Statement Try block"); e.printStackTrace(); }
		return hm;
	}
	

	
	
	/*
	public static void closeQuietly(AutoCloseable ... closeables) {
		/*
		 * Testing a function to close resources more effectively
		 *
	    for (AutoCloseable c : closeables) {
	        if (c != null) {
	            try {
	                c.close();
	            } catch (Exception e) {
	                System.err.println("Error in closeQuietly...");
	            }
	        }
	    }
	}

	public HashMap<Integer,String> getDescriptionsContainingDegrees() {
		/*
		 * For cases where the latitude/longitude is null but the description contains locational information
		 * Usually of the format
		 * Location: 152�00\'00\" E, 27�24\'00\" S;
		 *
		HashMap<Integer,String> descriptions = new HashMap<>();
		
		String sql = "SELECT anps_id,description FROM " + TABLE_NAME + " WHERE latitude IS NULL AND longitude IS NULL AND TLCM_Latitude IS NULL AND TLCM_Longitude IS NULL AND description LIKE '%�%' AND description LIKE '%Location: %';"; //has degrees symbol in it
		
		//Attempt Connection and SQL
		System.out.println("Attempting Connection...");
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {

			//Report success
			System.out.println("Connected to DB!");
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				while(rs.next()) {
					descriptions.put(rs.getInt(1),rs.getString(2)); //the 8th string in the result set is the description
				}
			} catch (Exception e) {System.err.println("Error in the ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Connection/Statement Try block"); e.printStackTrace(); }
		
		
		return descriptions;
	}
	
	public ArrayList<String> debugTable() {
		/*
		 * Quick debug function to make sure that Columns were inserted correctly
		 *
		//Container and return value
		ArrayList<String> rows = new ArrayList<>();
		
		//SQL - //Select data from the table
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE TLCM_Longitude IS NOT NULL limit 50;";

		//Attempt Connection and SQL - try with resources will close the connection/stmt/rs
		System.out.println("Attempting Connection...");
		try (Connection conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
			Statement stmt = conn.createStatement()) {

			//Report success
			System.out.println("Connected to DB!");
			
			//try Results set
			try (ResultSet rs = stmt.executeQuery(sql);) {
				//get number of columns
				ResultSetMetaData rsmd = rs.getMetaData();
				int numColumns = rsmd.getColumnCount();
				
				while (rs.next()) {
					String line = "";
					for (int i = 1; i <= numColumns; i++) {
						line += String.format("%-15.15s  ", rs.getString(i));
					}
					rows.add(line);
				}
			} catch (Exception e) {System.err.println("Error in the ResultSet Try block"); e.printStackTrace(); }
		} catch (Exception e) { System.err.println("Error in the Connection/Statement Try block"); e.printStackTrace(); }
		return rows;
	}
	*/
	
}
