package conversion;

import coordinates.Coordinates;

/*
 * Benjamin McDonnell for TLCMap Project 2019
 * University of Newcastle Australia
 */

public class DegreesToDecimals {
	public DegreesToDecimals() {
		
	}
	
	public double degreesToDecimals(double degrees, double minutes, double seconds) {
		/*
		 * Basic formula for degrees to decimals
		 */
		return degrees + (minutes/60.0) + (seconds/3600.0);
	}
	
	public double degreesToDecimals(String degrees, String minutes, String seconds) {
		/*
		 * Overloaded to take String inputs - ASSUMES that all input strings contain only numerical characters (numbers, points, negative signs)
		 */
		return Double.parseDouble(degrees) + (Double.parseDouble(minutes)/60.0) + (Double.parseDouble(seconds)/3600.0);
	}
	
	public Coordinates GDA94ToWGS84(Coordinates gda94) {
		/*
		 * Converting from the GDA94 coordinate system to WGS84
		 * For a Coordinates Object
		 */
		double wgsLatitude = 0;
		double wgsLongitude = 0;
		
		return new Coordinates(wgsLatitude,wgsLongitude);
	}
	
	/*********************************
	 *   DEBUG OR OUTDATED METHODS   *
	 *********************************/
	
	/*
	public double convertDegreesToDecimalsWithPunctuation(String text) {
		/*
		 * The text fed to this file is the text after Location and should simply look like 149°44\'00\" E, 26°21\'00\" S;
		 * This is performed once for the lat and once for the long
		 * 
		 * CASES PRESENT IN CSV
		 * 149°44\'00\" E, 
		 * 26°21\'00\" S; 
		 * 167° 57\' 4  
		 * -29° 7\' 13'
		 * Some other cases.... Sometimes the Lat ends in " or ' or , or the end of the column
		 *
		text = text.replace("\\",""); //Remove backslashes 
		text = text.replace("-",""); //strip negatives
		
		double degrees,decimals,minutes,seconds;
		degrees = decimals = minutes = seconds = 0;
		
		try {
			int start = 0;
			int end = text.indexOf("�"); //all cases we know of have degree symbol after the deg
			degrees = Double.parseDouble(text.substring(start,end));
			
			start = end+1;
			end = text.indexOf("\'"); //all cases we know of have minutes symbol after the minutes
			minutes = Double.parseDouble(text.substring(start,end));
			
			start = end+1;
			end = text.indexOf("\""); //several odd cases (" OR ' OR , OR end of col)
			if (end == -1) {end = text.indexOf("'",start);}
			if (end == -1) {end = text.indexOf(",",start);}
			if (end == -1) {end = text.length();}
			seconds = Double.parseDouble(text.substring(start,end));
		} catch(Exception e) { System.err.println(text); }
		
		decimals = degrees + (minutes/60) + (seconds/3600); //Degrees to Decimals formula
		
		return decimals;
	}
	*/
	
	/*
	public double convertDegreesToDecimalsGNR(String text) {
		/*
		 * Text input is Degrees in the format of "-31  14  30" 
		 *
		if (text.startsWith("-")) {
			
		}
	} */
}
