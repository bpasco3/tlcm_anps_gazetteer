package text;

import conversion.*;
import coordinates.*;

/*
 * Benjamin McDonnell for TLCMap Project 2019
 * University of Newcastle Australia
 */

public class Cases {
	/*
	 * Class to get the locational data from the description section of the TLCMap DB Register
	 * Case 1 references cases where regex SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: %
	 * This returns 108182 results which we will split into 7 subcases A through G, with 14 outliers
	 */
	public Cases() {}
	
	public Coordinates case1A(String description) {
		/*
		 * Class to return the pair of anps_id (PK) and the associated normalized coordinates
		 * Takes in the entire description section
		 * Input matches the regex SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: %�%\'%" E, %�%\'%" S;%'
		 * Returned Description is of the form Alternative Name: --; Location: 151�51'48" E, 27�42'28" S; QLD Comm...
		 * This returns 42489 results
		 * Note Longitude is first in the input string
		 */
		int longStart = description.indexOf("Location: ") + 10;
		int longEnd = description.indexOf("\"", longStart) + 1; //the first double quote after start position
		int latStart = description.indexOf(", ", longEnd) + 2; //first index of ", " after the longitude
		int latEnd = description.indexOf("\"",latStart) + 1; //First double quote after latStart

		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);

		String latDegrees = latitude.substring(0,latitude.indexOf("�"));
		String latMinutes = latitude.substring(latitude.indexOf("�")+1,latitude.indexOf("\'"));
		String latSeconds = latitude.substring(latitude.indexOf("\'")+1,latitude.indexOf("\""));
		
		String longDegrees = longitude.substring(0,longitude.indexOf("�"));
		String longMinutes = longitude.substring(longitude.indexOf("�")+1,longitude.indexOf("\'"));
		String longSeconds = longitude.substring(longitude.indexOf("\'")+1,longitude.indexOf("\""));
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		double latitudeDecimal = -d2d.degreesToDecimals(latDegrees, latMinutes, latSeconds); //Make negative, we are south of the equator
		double longitudeDecimal = d2d.degreesToDecimals(longDegrees, longMinutes, longSeconds);
		
		return new Coordinates(latitudeDecimal,longitudeDecimal);
		
	}
	
	public Coordinates case1B(String description) {
		/*
		 * Input matches the regex SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___.%, -__%'
		 * Returned Description is of the form Status: Official; Location: 118.1217, -34.79111
		 * This returns 50938 results
		 * Note Longitude is first in the input string
		 */
		int longStart = description.indexOf("Location: ") + 10; //Plus 10 so we start after the space
		int longEnd = description.indexOf(",",longStart); // substring will not include the final char (the comma)
		int latStart = description.indexOf("-",longEnd); //include the minus
		int latEnd = description.length(); // end should be the end of latitude
		
		double longitude = Double.parseDouble(description.substring(longStart,longEnd)); //already in negative
		double latitude = Double.parseDouble(description.substring(latStart,latEnd));
		
		return new Coordinates(latitude,longitude);
	}
	
	public Coordinates case1C(String description) {
		/*
		 * Input matches the regex SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: -__  __  __,  ___  __  __;%'
		 * Returned Description is of the form ... Location: -33  45  05,  151  01  34; ... Location: -28  47  38,  153  35  39;
		 * This returns 1053 results
		 * Note Latitude is actually first this time
		 */
		
		description=description.replaceAll("-", "");
		int latStart = description.indexOf("Location: ") + 10;
		int latEnd = description.indexOf(",", latStart);
		int longStart = latEnd + 2;
		int longEnd = description.indexOf(";",longStart);
		
		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);
		
		String[] latSplit = latitude.split("  ");
		String[] longSplit = longitude.split("  ");
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		double latitudeDecimal = -d2d.degreesToDecimals(latSplit[0],latSplit[1],latSplit[2]);
		double longitudeDecimal = d2d.degreesToDecimals(longSplit[0],longSplit[1],longSplit[2]);
		
		return new Coordinates(latitudeDecimal,longitudeDecimal);
	}
	
	public Coordinates case1D(String description) {
		/*
		 * Input matches the regex SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___� %\' %,%'
		 * Returned Description is of the form Location: 167� 55' 16, -29� 0' 34
		 * ONE UNIQUE CASE: Location: 167� 56' 15, -29� 0' 50. Volcanic mounta...
		 * This returns 93 results
		 * Note Longitude is first
		 */
		
		int longStart = description.indexOf("Location: ") + 10;
		int longEnd = description.indexOf(",", longStart);
		int latStart = longEnd + 3;
		int latEnd = description.indexOf(".",latStart);	//index of fullstop
		if (latEnd == -1) latEnd = description.indexOf("\n",latStart);
		if (latEnd == -1) latEnd = description.indexOf(";",latStart);
		if (latEnd == -1) latEnd = description.length();	//if fullstop doesnt exist (ie most cases), use the end of the string instead
		
		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);
		
		String[] latSplit = latitude.split(" ");
		String[] longSplit = longitude.split(" ");
		
		latSplit[0] = latSplit[0].replaceAll("�", "");
		latSplit[1] = latSplit[1].replaceAll("\'", "");
		longSplit[0] = longSplit[0].replaceAll("�", "");
		longSplit[1] = longSplit[1].replaceAll("\'", ""); //Strip away the � and '
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		double latitudeDecimal = -d2d.degreesToDecimals(latSplit[0],latSplit[1],latSplit[2]);
		double longitudeDecimal = d2d.degreesToDecimals(longSplit[0],longSplit[1],longSplit[2]);
		
		return new Coordinates(latitudeDecimal,longitudeDecimal);
	}
	
	public Coordinates case1G(String description) {
		/*
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{2,3}.?[0-9]{1,8}, -[0-9]{2}.?[0-9]{1,8}')
		 */
		int longStart = description.indexOf("Location: ") + 10; //Plus 10 so we start after the space
		int longEnd = description.indexOf(",",longStart); // substring will not include the final char (the comma)
		int latStart = description.indexOf("-",longEnd)+1; //DONT include the minus
		int latEnd = description.indexOf(".",latStart+3);	//FIXED DECIMAL STRIPPING ERROR?
		if (latEnd == -1) latEnd = description.length(); // end should be the end of latitude
		
		double longitude = Double.parseDouble(description.substring(longStart,longEnd)); //already in negative
		double latitude = -Double.parseDouble(description.substring(latStart,latEnd));
		
		return new Coordinates(latitude,longitude);
	}
	
	//Case 1E to 1G		10,610 outliers!
	
	//Case 2A Form of -31 09 54								735 results
	//Case 2B Form of 31� 31' 37" S							326 results
	//Case 2C Form of Already in accurate decimal form		87179 results!
	//Case 2D to 2G    378 outliers
	
	//Case 3A
	public Coordinates gnr_nsw(String latitude, String longitude) {
		/*
		 * From the GNR for NSW
		 * -33  40  54
		 * 151  16  04
		 */
		latitude = latitude.replaceAll("-", ""); // strip the negative, we add it later
		String[] latSplit = latitude.split("  "); //split around double space
		String[] longSplit = longitude.split("  ");
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		double latitudeDecimal = -d2d.degreesToDecimals(latSplit[0],latSplit[1],latSplit[2]); //enforce negative
		double longitudeDecimal = d2d.degreesToDecimals(longSplit[0],longSplit[1],longSplit[2]);
		
		return new Coordinates(latitudeDecimal,longitudeDecimal);
	}
	
	public Coordinates gnr_tas(String registerID, String gnrID, String latitude, String longitude) {
		/*
		 * From the GNR for TAS
		 * Register ID and State ID match, but are of differening forms
		 * registerID = 753M	gnrID = TAS00753
		 * Return null if they dont match, or parse the strings to double and return coordinates if they do match
		 */
		if (registerID == null || gnrID == null) return null;
		String a = registerID.replaceAll("[a-zA-Z]", ""); //Strip all letters
		String b = gnrID.replaceAll("[a-zA-Z]", ""); //Strip all letters
		b = b.replaceFirst("^0+(?!$)", "");
		
		if (a.equals(b)) {
			return new Coordinates(Double.parseDouble(latitude),Double.parseDouble(longitude));
		}
		return null;
		
	}
	
	public Coordinates case4A(String description) {
		/*
		 * Description contains locational data but in odd format
		 * Postcode: 7140; Long.: 152.606674194335; Lat.: -26.9663887023925; 100K Map: 9444
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND state_code = 'QLD' AND description REGEXP('Long.:')
		 * 1782 results
		 */
		int longStart = description.indexOf("Long.: ")+7; 
		int longEnd = description.indexOf(";",longStart);
		int latStart = description.indexOf("-",longEnd)+1;
		int latEnd = description.indexOf(";",latStart);
		
		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);
		
		return new Coordinates(-Double.parseDouble(latitude),Double.parseDouble(longitude));
	}
	
	public Coordinates case4E(String description) {
		/*
		 * Description contains locational data but in odd format
		 *	AND description LIKE '%�%'
		 *	LATITUDE: 33� 59' S \nLONGITUDE: 122� 13' E
		 *	19 results
		 *
		 * One particular nasty
		 * Hill, 60 metres high, 11 km NNE of Wyndham; 15� 23' S, 128� 09' E
		 */
		
		description = description.replaceAll(" ", "");  //LATITUDE:33�59'S\nLONGITUDE:122�13'E
		
		int latStart = description.indexOf("LATITUDE:")+9; 
		if (latStart == 8) {latStart = description.indexOf(";")+1;}
		int latEnd = description.indexOf("'");						
		int longStart = description.indexOf("LONGITUDE:")+10;
		if (longStart == 9) {longStart = description.indexOf(",",latEnd)+1;}
		int longEnd = description.indexOf("'",longStart);			
		
		String latitude = description.substring(latStart,latEnd);    //33�59
		String longitude = description.substring(longStart,longEnd); //122�13
		
		String latDeg = latitude.substring(0,2);
		String latMin = latitude.substring(3);
		String longDeg = longitude.substring(0,3);
		String longMin = longitude.substring(4);
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		return new Coordinates(-d2d.degreesToDecimals(latDeg, latMin, "00"),d2d.degreesToDecimals(longDeg, longMin, "00"));
	}
	
	public Coordinates caseOA(String description) {
		/*
		 * Outlier Case with decimalised latlong in description in wrong order
		 * official; 145.202222222222, -37.8361111111111
		 * SELECT anps_id,description FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('; [0-9]{3}.[0-9]{1,15}, -[0-9]{2}.[0-9]{1,15}')
		 * 35000+ results
		 */
		int longStart = description.lastIndexOf(";")+2; //use last index of as some contain more info before 'official;'
		int longEnd = description.indexOf(",",longStart);
		int latStart = description.indexOf("-",longEnd)+1; //dont include the negative, we will make the whole decimal negative later
		int latEnd = description.indexOf(".",latStart+3);
		if (latEnd == -1) latEnd = description.indexOf("%n",latStart);
		if (latEnd == -1) latEnd = description.indexOf(" ",latStart);
		if (latEnd == -1) latEnd = description.length();
		
		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);
		
		return new Coordinates(-Double.parseDouble(latitude),Double.parseDouble(longitude));
	}

	public Coordinates caseOB(String latitude,String longitude) {
		/*
		 * Outlier case of the form
		 * lat: 31� 31' 15.1" S				'space S' is optional
		 * long: 159� 04' 27.4" E			2 cases where longitude doesnt have deg symbol
		 * WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND latitude REGEXP('[0-9]{2}� [0-9]{2}\' [0-9]{2}.?[0-9]{0,1}\"[[:space:]]?S?'
		 * 156 results
		 * 
		 * outlier case where longitude is of form      159 02' 42.7" E		after strip     15902'42.7"E
		 */
		latitude = latitude.replaceAll(" ", ""); //31�31'15.1"S
		longitude = longitude.replaceAll(" ",""); //159�04'27.4"E
		
		String latDeg = latitude.substring(0,latitude.indexOf("�"));
		String latMin = latitude.substring(latitude.indexOf("�")+1,latitude.indexOf("'"));
		String latSec = latitude.substring(latitude.indexOf("'")+1,latitude.indexOf("\""));
		
		
		int longDegEnd = longitude.indexOf("�");
		int longMinStart = longDegEnd + 1;
		if (longDegEnd == -1) {
			longDegEnd = 3;
			longMinStart = 3;
		}
		
		String longDeg = longitude.substring(0, longDegEnd);
		String longMin = longitude.substring(longMinStart,longitude.indexOf("'"));
		String longSec = longitude.substring(longitude.indexOf("'")+1,longitude.indexOf("\""));
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		return new Coordinates(-d2d.degreesToDecimals(latDeg, latMin, latSec),d2d.degreesToDecimals(longDeg, longMin, longSec));
	}
	
	public Coordinates caseOC(String description) {
		/*
		 * description of the form
		 * Status: Official; Other Names: -; Location:149� 7' 0 / -35� 17' 40
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location:[0-9]{3}� [0-9]{1,2}\' [0-9]{1,2} / -[0-9]{2}� [0-9]{1,2}\' [0-9]{1,2}')
		 * 864 results
		 */
		
		int longStart = description.indexOf("Location:")+9;
		int longEnd = description.indexOf(" /",longStart);
		int latStart = description.indexOf("-",longEnd)+1; //ignore negative
		int latEnd = description.length();
		
		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);
		
		String latDeg = latitude.substring(0,2);
		String latMin = latitude.substring(latitude.indexOf("�")+2,latitude.indexOf("'"));
		String latSec = latitude.substring(latitude.indexOf("'")+2,latitude.length());
		
		String longDeg = longitude.substring(0,3);
		String longMin = longitude.substring(longitude.indexOf("�")+2,longitude.indexOf("'"));
		String longSec = longitude.substring(longitude.indexOf("'")+2,longitude.length());
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		return new Coordinates(-d2d.degreesToDecimals(latDeg, latMin, latSec),d2d.degreesToDecimals(longDeg, longMin, longSec));
	}
	
	public Coordinates caseOD(String description) {
		/*
		 * description of form
		 * Status: Official; Other Names: -; Location:133� 53' 0, -22� 12' 0
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location:[0-9]{3}� [0-9]{1,2}\' [0-9]{1,2}, -[0-9]{2}� [0-9]{1,2}\' [0-9]{1,2}$')
		 * 12,604 results
		 */
		
		int longStart = description.indexOf("Location:")+9;
		int longEnd = description.indexOf(",",longStart); 										//133� 53' 0
		int latStart = description.indexOf("-",longEnd)+1; //ignore negative
		int latEnd = description.length();														//22� 12' 0
		
		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);
		
		String latDeg = latitude.substring(0,2);												//22
		String latMin = latitude.substring(latitude.indexOf("�")+2,latitude.indexOf("'"));		//12
		String latSec = latitude.substring(latitude.indexOf("'")+2,latitude.length());			//0
		
		String longDeg = longitude.substring(0,3);
		String longMin = longitude.substring(longitude.indexOf("�")+2,longitude.indexOf("'"));
		String longSec = longitude.substring(longitude.indexOf("'")+2,longitude.length());
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		return new Coordinates(-d2d.degreesToDecimals(latDeg, latMin, latSec),d2d.degreesToDecimals(longDeg, longMin, longSec));
		
	}
	
	public Coordinates caseOE(String description) {
		/*
		 * description of form
		 * Status: Official; Other Names: --; Location: 150� 37 59, -35� 10 0
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{3}� [0-9]{1,2} [0-9]{1,2}, -[0-9]{2}� [0-9]{1,2} [0-9]{1,2}')
		 * 42 results
		 */
		
		int longStart = description.indexOf("Location:")+10;
		int longEnd = description.indexOf(",",longStart);
		int latStart = description.indexOf("-",longEnd)+1; //ignore negative
		int latEnd = description.length();
		
		String latitude = description.substring(latStart,latEnd);
		String longitude = description.substring(longStart,longEnd);
		
		String latDeg = latitude.substring(0,2);												
		String latMin = latitude.substring(latitude.indexOf("�")+2,latitude.indexOf(" ",latitude.indexOf("�")+2));		
		String latSec = latitude.substring(latitude.indexOf(" ",latitude.indexOf("�")+2)+1,latitude.length());			
		
		String longDeg = longitude.substring(0,3);
		String longMin = longitude.substring(longitude.indexOf("�")+2,longitude.indexOf(" ",longitude.indexOf("�")+2));
		String longSec = longitude.substring(longitude.indexOf(" ",longitude.indexOf("�")+2)+1,longitude.length());
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		return new Coordinates(-d2d.degreesToDecimals(latDeg, latMin, latSec),d2d.degreesToDecimals(longDeg, longMin, longSec));
		
	}
	
	public Coordinates caseOF(String description) {
	/*
	 * description of form
	 * Current Name: MORETON BAY; Location: 153�15'34" E, 27�17'24" S.			optional fullstop
	 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{3}�[0-9]{1,2}\'[0-9]{1,2}\" E, [0-9]{2}�[0-9]{1,2}\'[0-9]{1,2}\" S.?')
	 * 3 results
	 */
		int longStart = description.indexOf("Location:")+10;
		int longEnd = description.indexOf("\"",longStart);
		int latStart = description.indexOf(", ",longEnd)+2;
		int latEnd = description.indexOf("\"",latStart);
		
		String latitude = description.substring(latStart,latEnd);			//153�15'34
		String longitude = description.substring(longStart,longEnd);		//27�17'24
		
		String latDeg = latitude.substring(0,2);												
		String latMin = latitude.substring(latitude.indexOf("�")+2,latitude.indexOf("'"));		
		String latSec = latitude.substring(latitude.indexOf("'")+1,latitude.length());			
		
		String longDeg = longitude.substring(0,3);
		String longMin = longitude.substring(longitude.indexOf("�")+2,longitude.indexOf("'"));
		String longSec = longitude.substring(longitude.indexOf("'")+1,longitude.length());
		
		DegreesToDecimals d2d = new DegreesToDecimals();
		
		return new Coordinates(-d2d.degreesToDecimals(latDeg, latMin, latSec),d2d.degreesToDecimals(longDeg, longMin, longSec));
		
	}
	
	public Coordinates caseOG(String description) {
		/*
		 * description of form
		 * variant of Phillip Island, location: 167� 57' 4, -29� 7' 13
		 * SELECT * FROM `register` WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description like 'variant of Phillip Island, location: 167� 57\' 4, -29� 7\' 13'
		 * 1 results
		 */
			int longStart = description.indexOf("location:")+10;
			int longEnd = description.indexOf(",",longStart);
			int latStart = longEnd+3; //ignore negative
			int latEnd = description.length();
			
			String latitude = description.substring(latStart,latEnd);
			String longitude = description.substring(longStart,longEnd);
			
			String latDeg = latitude.substring(0,2);												
			String latMin = latitude.substring(4,5);		
			String latSec = latitude.substring(7,9);		
			
			String longDeg = longitude.substring(0,3);
			String longMin = longitude.substring(5,7);
			String longSec = longitude.substring(9,10);
			
			DegreesToDecimals d2d = new DegreesToDecimals();
			
			return new Coordinates(-d2d.degreesToDecimals(latDeg, latMin, latSec),d2d.degreesToDecimals(longDeg, longMin, longSec));
			
		}
	
	public String stateId(String description) {
		/*
		 * description of form
		 * variant of Providence Bay (State ID 47480)
		 * we just want the state id
		 */
			description = description.toLowerCase();
			int start = description.indexOf("(state id ")+10;
			int end = description.indexOf(")",start);
			
			return description.substring(start,end);
		}
	
	public String anpsId(String description) {
		/*
		 * Discontinued. See ANPS ID 20161 �State ID 81103
		 * original form of Paradise, TAS ID 1176Q, ANPS ID 81428
		 * A bend in the Cudgegong River, near Cudgegong (ANPS ID 20032)
		 * See also Central Mount Sturt, ANPS ID 92727; Central Mount Stuart, ANPS ID 234103
		 * varian of Middle Head, ANPS ID 46045, State ID 37723
		 * seems to end in space, comma, semicolon,parenthesis, or end of string
		 */
		description = description.toLowerCase();
		int start = description.indexOf("anps id")+7;
		if (description.charAt(start) == ' ') start++;
		if (description.length() > start+7) {
			description = description.substring(start,start+7);
			start = 0;
		}
		
		int end = description.indexOf(';',start);
		if (end == -1) end = description.indexOf(',',start);
		if (end == -1) end = description.indexOf(')',start);
		if (end == -1) end = description.indexOf(' ',start);
		if (end == -1) end = description.indexOf('\n',start);
		if (end == -1) end = description.length();
		
		String sub = description.substring(start,end);
		sub = sub.replaceAll("[^0-9]","");
		
		return sub;
	}
}
