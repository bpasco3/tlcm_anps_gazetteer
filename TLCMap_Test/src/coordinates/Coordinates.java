/*
 * Benjamin McDonnell for TLCMap Project 2019
 * University of Newcastle Australia
 */

package coordinates;
/*
 * Coordinates object, holds id, latitude, and longitude as decimal values
 */

public class Coordinates {
	private double latitude;
	private double longitude;
	
	public Coordinates(double latitude, double longitude) {
		this.latitude = (latitude < 0) ? latitude : - latitude; //make latitude negative as we are south of the equator
		this.longitude = longitude;
	}
	
	//Get
	public double getLatitude() { return latitude; }
	public double getLongitude() { return longitude; }
	
	//Set
	public void setLatitude(double latitude) { this.latitude = latitude; }
	public void setLongitude(double longitude) { this.longitude = longitude; }
}
