package kml;
/*
 * Benjamin McDonnell for TLCMap Project 2019
 * University of Newcastle Australia
 */
public class Placemark {
	/*
	 * Object to hold data for our KML placemark
	 * r.anps_id, r.placename, r.state_id, r.description, r.state_code, r.TLCM_Latitude, r.TLCM_Longitude, r.flag, r.google_maps_link, r.ORIGINAL_DATA_SOURCE, 
	 * 				anps_source_id, anps_source_type, anps_source_title, anps_source_author, anps_source_isbn, anps_source_publisher, anps_source_place, anps_source_date, anps_source_location
	 */
	int anps_id;
	String placename;
	String state_id;
	String description;
	String state_code;
	double TLCM_Latitude;
	double TLCM_Longitude;
	String flag;
	String google_maps_link;
	String original_data_source;
	
	int anps_source_id;
	String anps_source_type;
	String anps_source_title;
	String anps_source_author;
	String anps_source_isbn;
	String anps_source_publisher;
	String anps_source_place;
	String anps_source_date;
	String anps_source_location;
	
	public Placemark() {}
	
	public Placemark(int anps_id, String placename,String state_id,String description,String state_code,double TLCM_Latitude,double TLCM_Longitude,String flag,String google_maps_link,String original_data_source,int anps_source_id,
			String anps_source_type,String anps_source_title,String anps_source_author,String anps_source_isbn,String anps_source_publisher,String anps_source_place,String anps_source_date,String anps_source_location) {
		this.anps_id = anps_id;
		this.placename = placename;
		this.state_id = state_id;
		this.description = description;
		this.state_code = state_code;
		this.TLCM_Latitude = TLCM_Latitude;
		this.TLCM_Longitude = TLCM_Longitude;
		this.flag = flag;
		this.google_maps_link = google_maps_link;
		this.original_data_source = original_data_source;
		
		this.anps_source_id = anps_source_id;
		this.anps_source_type = anps_source_type;
		this.anps_source_title = anps_source_title;
		this.anps_source_author = anps_source_author;
		this.anps_source_isbn = anps_source_isbn;
		this.anps_source_publisher = anps_source_publisher;
		this.anps_source_place = anps_source_place;
		this.anps_source_date = anps_source_date;
		this.anps_source_location = anps_source_location;
	}

	public int getAnps_id() {
		return anps_id;
	}

	public void setAnps_id(int anps_id) {
		this.anps_id = anps_id;
	}

	public String getPlacename() {
		return placename;
	}

	public void setPlacename(String placename) {
		this.placename = placename;
	}

	public String getState_id() {
		return state_id;
	}

	public void setState_id(String state_id) {
		this.state_id = state_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getState_code() {
		return state_code;
	}

	public void setState_code(String state_code) {
		this.state_code = state_code;
	}

	public double getTLCM_Latitude() {
		return TLCM_Latitude;
	}

	public void setTLCM_Latitude(double tLCM_Latitude) {
		TLCM_Latitude = tLCM_Latitude;
	}

	public double getTLCM_Longitude() {
		return TLCM_Longitude;
	}

	public void setTLCM_Longitude(double tLCM_Longitude) {
		TLCM_Longitude = tLCM_Longitude;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getGoogle_maps_link() {
		return google_maps_link;
	}

	public void setGoogle_maps_link(String google_maps_link) {
		this.google_maps_link = google_maps_link;
	}

	public String getOriginal_data_source() {
		return original_data_source;
	}

	public void setOriginal_data_source(String original_data_source) {
		this.original_data_source = original_data_source;
	}

	public int getAnps_source_id() {
		return anps_source_id;
	}

	public void setAnps_source_id(int anps_source_id) {
		this.anps_source_id = anps_source_id;
	}

	public String getAnps_source_type() {
		return anps_source_type;
	}

	public void setAnps_source_type(String anps_source_type) {
		this.anps_source_type = anps_source_type;
	}

	public String getAnps_source_title() {
		return anps_source_title;
	}

	public void setAnps_source_title(String anps_source_title) {
		this.anps_source_title = anps_source_title;
	}

	public String getAnps_source_author() {
		return anps_source_author;
	}

	public void setAnps_source_author(String anps_source_author) {
		this.anps_source_author = anps_source_author;
	}

	public String getAnps_source_isbn() {
		return anps_source_isbn;
	}

	public void setAnps_source_isbn(String anps_source_isbn) {
		this.anps_source_isbn = anps_source_isbn;
	}

	public String getAnps_source_publisher() {
		return anps_source_publisher;
	}

	public void setAnps_source_publisher(String anps_source_publisher) {
		this.anps_source_publisher = anps_source_publisher;
	}

	public String getAnps_source_place() {
		return anps_source_place;
	}

	public void setAnps_source_place(String anps_source_place) {
		this.anps_source_place = anps_source_place;
	}

	public String getAnps_source_date() {
		return anps_source_date;
	}

	public void setAnps_source_date(String anps_source_date) {
		this.anps_source_date = anps_source_date;
	}

	public String getAnps_source_location() {
		return anps_source_location;
	}

	public void setAnps_source_location(String anps_source_location) {
		this.anps_source_location = anps_source_location;
	}
}
