package kml;

import java.util.ArrayList;

/*
 * Benjamin McDonnell for TLCMap Project 2019
 * University of Newcastle Australia
 */
public class KMLBuilder {

	public KMLBuilder() {}
	
	public ArrayList<String> build(ArrayList<Placemark> placemarks) {
		/*
		 * Takes a bunch of data lines of the form
		 * anps_id, placename, state_id, description, state_code, TLCM_Latitude, TLCM_Longitude, flag, google_maps_link, original_source, anps_source
		 * and creates kml style entries for each
		 * also handles all of the initial structuring of our kml file
		 * 
		 * We can export the csv manually from phpmyadmin after using the following sql command:
		 * 
		 */
		
		ArrayList<String> output = new ArrayList<>();
		
		//Setup
		output.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		output.add("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
		output.add("\t<Document>");
		
		//For each Row in input
		for (Placemark pl : placemarks) {
			
			//Use some method to pluck data from each input line into an appropriate variable?
			
			//adding lines to the kml arraylist
			output.add("\t\t<Placemark>");
			
			output.add("\t\t\t<name><![CDATA["+pl.getPlacename()+"]]></name>"); //insert placename
			//output.add("\t\t\t<description><![CDATA["+pl.getDescription()+"]]></description>"); //insert description
			
			output.add("\t\t\t<Point>");
			output.add("\t\t\t\t<coordinates>"+pl.getTLCM_Longitude()+","+pl.getTLCM_Latitude()+"</coordinates>"); //insert Lat and Long
			output.add("\t\t\t</Point>");
			
			output.add("\t\t\t<ExtendedData>");
			
			output.add("\t\t\t\t<Data name = \"anps_id\">");
			output.add("\t\t\t\t\t<displayName>ANPS ID</displayName>");
			output.add("\t\t\t\t\t<value>"+pl.getAnps_id()+"</value>"); 	//insert anps_id
			output.add("\t\t\t\t</Data>");
			
			output.add("\t\t\t\t<Data name = \"state_id\">");
			output.add("\t\t\t\t\t<displayName>State ID</displayName>");
			output.add("\t\t\t\t\t<value><![CDATA["+pl.getState_id()+"]]></value>"); 	//insert state_id
			output.add("\t\t\t\t</Data>");
			
			output.add("\t\t\t\t<Data name = \"state\">");
			output.add("\t\t\t\t\t<displayName>State</displayName>");
			output.add("\t\t\t\t\t<value><![CDATA["+pl.getState_code()+"]]></value>"); 	//insert state_code
			output.add("\t\t\t\t</Data>");
			
			output.add("\t\t\t\t<Data name = \"flag\">");
			output.add("\t\t\t\t\t<displayName>Flag</displayName>");
			output.add("\t\t\t\t\t<value><![CDATA["+pl.getFlag()+"]]></value>"); 	//insert flag
			output.add("\t\t\t\t</Data>");
			
			output.add("\t\t\t\t<Data name = \"original_source\">");
			output.add("\t\t\t\t\t<displayName>Original Source</displayName>");
			output.add("\t\t\t\t\t<value><![CDATA["+pl.getOriginal_data_source()+"]]></value>"); 	//insert original_source
			output.add("\t\t\t\t</Data>");
			
			output.add("\t\t\t\t<Data name = \"description\">");
			output.add("\t\t\t\t\t<displayName>Description</displayName>");
			output.add("\t\t\t\t\t<value><![CDATA["+pl.getDescription()+"]]></value>"); 	//insert original_source
			output.add("\t\t\t\t</Data>");
			
			if (pl.getAnps_source_id() != 0) {
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_id\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_id</displayName>");
				output.add("\t\t\t\t\t\t\t<value>"+pl.getAnps_source_id()+"</value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_type\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_type</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_type()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_title\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_title</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_title()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_author\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_author</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_author()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_isbn\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_isbn</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_isbn()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_publisher\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_publisher</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_publisher()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_place\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_place</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_place()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_date\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_date</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_date()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
				output.add("\t\t\t\t\t\t<Data name = \"anps_source_location\">");
				output.add("\t\t\t\t\t\t\t<displayName>ANPS source_location</displayName>");
				output.add("\t\t\t\t\t\t\t<value><![CDATA["+pl.getAnps_source_location()+"]]></value>"); 	//insert original_source
				output.add("\t\t\t\t\t\t</Data>");
				
			}

			
			output.add("\t\t\t</ExtendedData>");
			output.add("\t\t</Placemark>");
		}
		
		//End
		output.add("\t</Document>");
		output.add("</kml>");
		
		return output;
	}
}
