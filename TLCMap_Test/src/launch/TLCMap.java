package launch;
/*
 * Benjamin McDonnell for TLCMap Project 2019
 * University of Newcastle Australia
 */

import db.*;

public class TLCMap {
	/*
	 * Launch class for our program
	 * Notes:
	 * 		Range in Australia Approximately:
	 * 			Latitude: -10 to -44 (SOUTH)
	 * 			Logitude: 113 to 154 (EAST)
	 * 
	 * KNOWN BUGS:
	 * 		Repeating the program will find an additional 72 results? Unsure why
	 * 		Kinda slow, look into multithreading?
	 * 		
	 * 
	 * PREREQUESITES:
	 * 		GNR tables loaded into the tlcmap database
	 * 		We are using the format 'gnr_statecode' eg gnr_nsw gnr_tas gnr_vic
	 * 
	 * 		Full list of tables we must add:
	 * 			gnr_nsw
	 * 			lga_tas
	 * 			aus_gaz
	 * 
	 * 		Perform this action with the mysql commands directly through the console
	 * 		source c:/users/bjm662/documents/tlc/sql/gnr_nsw.sql
	 * 		source c:/users/bjm662/documents/tlc/sql/lga_tas.sql
	 * 		source c:/users/bjm662/documents/tlc/sql/aus_gaz.sql
	 * 		
	 * 		
	 */

	public static void main(String[] args) {
		//JDBC Driver setup
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; //for mysql
		final String DB_URL = "jdbc:mariadb://localhost:3306/tlcmap2"; //server and database MAKE SURE YOU SET THIS TO THE CORRECT VALUE
		
		//DB Credentials
		final String USERNAME = "root";
		final String PASSWORD = "";
		final String TABLE_NAME = "register";
		
		//Welcome
		System.out.println("Welcome to the TLCMap Gazetteer program!");
		System.out.println("Program will now begin updates on the tlcmap register table to input decimalised locational data.");
		System.out.println("The entire process should take 2 to 10 minutes, please do not quit during the process.");
		System.out.println("\n---------------------------------------------------------------------------------------------------------");
		
		//Get the DBHelper class
		@SuppressWarnings("unused")
		DBHelper dbh = new DBHelper(JDBC_DRIVER,DB_URL,USERNAME,PASSWORD,TABLE_NAME);
		
		/*
		 * 
		//Drop the Columns that we will be adding (For cleanup)
		dbh.dropCoordinateColumns();
		
		//Add new columns for our new coordinates
		dbh.addCoordinateColumns();
		
		
		//Copy the Variables over from the gnr table
		dbh.copyGNR(); //FIXED!
		
		
		//
		//The remainder of the program will consider all the different cases and input the coordinates appropriately
		//
		
		*/
		
		/*
		
		//
		// Case 1 - latitude and longitude coluns are empty, but the location is written in the description in the form of 'Location: ...'
		// SQL: SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: %
		// Returned Description is of the form: Alternative Name: --; Location: 151�51'48" E, 27�42'28" S; QLD Comm...
		//
		
		//Subcase A - SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: %�%\'%" E, %�%\'%" S;%'
		dbh.addTLCMCoordinatesBatch(dbh.case1A(),"TLCM_Latitude","TLCM_Longitude","anps description section."); //42489 updates
		
		//Subcase B - SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___.%, -__%'
		dbh.addTLCMCoordinatesBatch(dbh.case1B(),"TLCM_Latitude","TLCM_Longitude","anps description section."); //50938 updates
		
		//Subcase C - SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: -__  __  __,  ___  __  __;%'
		dbh.addTLCMCoordinatesBatch(dbh.case1C(),"TLCM_Latitude","TLCM_Longitude","anps description section."); //1053 updates 
		
		//Subcase D - SELECT anps_id,description FROM register WHERE latitude IS NULL AND longitude IS NULL AND description LIKE '%Location: ___� %\' %,%'
		dbh.addTLCMCoordinatesBatch(dbh.case1D(),"TLCM_Latitude","TLCM_Longitude","anps description section."); //92 updates 
		
		//Subcase G - SELECT anps_id,description FROM register WHERE TLCM_Latitude IS NULL AND TLCM_GDA94_Latitude_Decimal IS NULL AND description REGEXP('Location: [0-9]{2,3}.?[0-9]{1,8}, -[0-9]{2}.?[0-9]{1,8}')
		dbh.addTLCMCoordinatesBatch(dbh.case1G(),"TLCM_Latitude","TLCM_Longitude","anps description section."); //61 updates
		
		
		
		
		//
		// Case 2 - Where latitude and longitude are not null
		//
		
		//SubcaseA
		dbh.addTLCMCoordinatesBatch(dbh.case2A(), "TLCM_Latitude","TLCM_Longitude","anps latitude and longitude columns."); // 252 updates
		
		//Subcase C - SELECT anps_id,description REGISTER WHERE latitude IS NOT NULL AND longitude IS NOT NULL AND latitude<>'' AND latitude LIKE '-__.%'
		dbh.addTLCMCoordinatesBatch(dbh.case2C(),"TLCM_Latitude","TLCM_Longitude","anps latitude and longitude columns."); //87179 updates
		
		
		
		
		
		//
		// Case 4 - Non NSW containing locational data in the description but in odd formats
		//
		
		//Subcase A 
		dbh.addTLCMCoordinatesBatch(dbh.case4A(), "TLCM_Latitude","TLCM_Longitude", "anps description section.");
		
		//Subcase E
		dbh.addTLCMCoordinatesBatch(dbh.case4E(), "TLCM_Latitude","TLCM_Longitude", "anps description section.");
		
		
		
		//
		// Case O - Outlier cases, ~15,948 that are not covered by the rest of this program
		// 
		
		
		//Subcase A
		dbh.addTLCMCoordinatesBatch(dbh.caseOA(), "TLCM_Latitude", "TLCM_Longitude","anps description section."); //7054      had error or input string: "38.7562499999999 \n143� 20� E"
		
		//Subcase B
		dbh.addTLCMCoordinatesBatch(dbh.caseOB(), "TLCM_Latitude", "TLCM_Longitude","anps latitude and longitude columns."); //344
		
		//Subcase C
		dbh.addTLCMCoordinatesBatch(dbh.caseOC(), "TLCM_Latitude", "TLCM_Longitude","anps description section."); //864
		
		//Subcase D
		dbh.addTLCMCoordinatesBatch(dbh.caseOD(), "TLCM_Latitude", "TLCM_Longitude","anps description section."); //12604
		
		//Subcase E
		dbh.addTLCMCoordinatesBatch(dbh.caseOE(), "TLCM_Latitude", "TLCM_Longitude","anps description section."); //42
		
		//Subcase F
		dbh.addTLCMCoordinatesBatch(dbh.caseOF(), "TLCM_Latitude", "TLCM_Longitude","anps description section."); //5
		
		//Subcase G
		dbh.addTLCMCoordinatesBatch(dbh.caseOG(), "TLCM_Latitude", "TLCM_Longitude","anps description section."); //1
		
		
		
		
		//
		// Related names cases
		// 261?
		//
		dbh.addTLCMCoordinatesBatch(dbh.related_names_1A(), "TLCM_Latitude", "TLCM_Longitude","Related Names Table linked this to another id."); //51
		dbh.addTLCMCoordinatesBatch(dbh.related_names_1B(), "TLCM_GDA94_Latitude_Decimal","TLCM_GDA94_Longitude_Decimal","Related Names Table linked this to another id.", "Uses GDA94 Coordinates instead of WGS84"); //0
		dbh.addTLCMCoordinatesBatch(dbh.related_names_2A(), "TLCM_Latitude", "TLCM_Longitude","Related Names Table linked this to another id."); //18
		dbh.addTLCMCoordinatesBatch(dbh.related_names_2B(), "TLCM_GDA94_Latitude_Decimal","TLCM_GDA94_Longitude_Decimal","Related Names Table linked this to another id.", "Uses GDA94 Coordinates instead of WGS84"); //0
		
		
		
		//
		// GNR CASES
		//
		
		//NSW GNR
		dbh.addTLCMCoordinatesBatch(dbh.gnr_nsw(),"TLCM_GDA94_Latitude_Decimal","TLCM_GDA94_Longitude_Decimal","NSW gnr", "Uses GDA94 Coordinates instead of WGS84"); //176362 updates?   82782 updates?
		
		//NSW gnr where the name+lga matches but the id does not
		dbh.addTLCMCoordinatesBatch(dbh.gnr_nsw_3(),"TLCM_GDA94_Latitude_Decimal","TLCM_GDA94_Longitude_Decimal","NSW gnr by matching the placename+lga combinations. Is this a sufficient identifier???","Uses GDA94 Coordinates instead of WGS84"); //  173 updates?
		
		//Gazetteer
		dbh.addTLCMCoordinatesBatch(dbh.gazetteer(), "TLCM_Latitude", "TLCM_Longitude", "Australian Gazetteer"); //45560
		
		//Tas lga
		dbh.addTLCMCoordinatesBatch(dbh.lga_tas(), "TLCM_Latitude", "TLCM_Longitude", "LGA centrepoints for Tasmania","Only accurate to LGA"); //541
		
		//VIC GNR
		dbh.addTLCMCoordinatesBatch(dbh.gnr_vic(), "TLCM_Latitude", "TLCM_Longitude", "VIC gnr"); //47 
		
		
		
		
		//StateIdA 
		dbh.addTLCMCoordinatesBatch(dbh.stateIdA(), "TLCM_Latitude", "TLCM_Longitude", "Description lists this as a variant of another stateId"); //64 results total
		//StateIdB
		dbh.addTLCMCoordinatesBatch(dbh.stateIdB(), "TLCM_GDA94_Latitude_Decimal","TLCM_GDA94_Longitude_Decimal", "Description lists this as a variant of another stateId", "Uses GDA94 Coordinates instead of WGS84"); 
		
		
		//anpsIdA
		dbh.addTLCMCoordinatesBatch(dbh.anpsIdA(), "TLCM_Latitude", "TLCM_Longitude", "Description lists this as a variant of another anpsId"); //25 results total
		//anpsIdB
		dbh.addTLCMCoordinatesBatch(dbh.anpsIdB(), "TLCM_GDA94_Latitude_Decimal","TLCM_GDA94_Longitude_Decimal", "Description lists this as a variant of another anpsId", "Uses GDA94 Coordinates instead of WGS84");
		
		
		//Google maps links
		dbh.insertGoogleLinks();
		
		//Flag data as being just a placeholder
		dbh.flagPlaceholderData();
		
		//Copy data over from the TLCM_GDA columns to the TLCM columns
		dbh.copyOverGDA();
		
		//Populate the ORIGINAL_DATA_SOURCE column
		dbh.insertOriginalDataSource();
		
		*/
		//Generate KML
		dbh.buildKML();
		
		
		
		//Report Success
		System.out.println("\n---------------------------------------------------------------------------------------------------------\n\n"
				+ "Program complete! Have a nice day :)");
	}

}
