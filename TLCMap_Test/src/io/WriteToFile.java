package io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class WriteToFile {
	String outputFilename = "";
	String folder = "io";
	File file;
	
	public WriteToFile() {}
	
	public void requestOutputFilename() {
		System.out.println("Please enter a name for your new KML output file (eg out.kml): ");
		Scanner sc = new Scanner(System.in);
		outputFilename = sc.nextLine();
		if (!outputFilename.endsWith(".kml")) {outputFilename += ".kml";} //add the extension if not present
		file = new File(folder,outputFilename);
	}
	
	public void write(ArrayList<String> lines) {
		//Write file
		System.out.println("\nWriting to file...");
		try (BufferedWriter bw=new BufferedWriter(new FileWriter(file))) {
			for (String line : lines) {
				bw.write(line);
				bw.newLine();
			}
			System.out.println("Writing to file complete!");
		}catch(Exception e) {e.printStackTrace();}
	}
}
